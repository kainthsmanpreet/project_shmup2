#include<vector>
#include "State.h"
#include "Input.h"

#ifndef STATE_MANAGER_H
#define MAX_STATES 100
#define STATE_MANAGER_H

class StateManager : public StateManagerAbstract {
public:
	StateManager();
	~StateManager();

	/*Add new state to our non active list*/
	void AddNewState(StateId, State *);

	/*Push a new state by sending the class instance*/
	void PushState(State *);
	
	/*Push a state by sending the class id*/
	void PushState(StateId);

	/*Remove the topmost state*/
	void PopState();

	/*Clear the entire stack and push a new state*/
	void ResetStackAndPush(State *);

	/*Change the topmost state*/
	void SetState(State *);

	void SetState(StateId);

	/*Helper function for initializing states*/
	void InitState(State *);

	/*Update() implementation*/
	void Update();

	void Update(float dt);

	void SetInputHandler(InputHandler*);

	void SetGraphicsSystem(Graphics*);

	bool mIsRunning;

private:

	std::vector<State*> mActiveStack ;
	State*				mStateStack[MAX_STATES];

	InputHandler*		mpInputHandler;
	Graphics*			mpGraphics;
};

#endif