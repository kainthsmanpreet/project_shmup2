#pragma once
class EntityUtil;
namespace Ogre {
	class Vector3;
}
enum WeaponType{
	WT_RAPID_FIRE,
	WT_SPREAD,
	WT_HOMING,
	WT_LASER,
	WT_LAST
};

enum BulletType {
	B_RAPID_LASER,
	B_SPREAD,
	B_MISSILE,
	B_LASER,
	B_LAST
};

void ShootRapidLaser(EntityUtil*, Ogre::Vector3);
void ShootHomingMissile(EntityUtil*, Ogre::Vector3);
void ShootSpread(EntityUtil*, Ogre::Vector3);
void ShootLaser(EntityUtil*, Ogre::Vector3);