#pragma once
#include <iostream>
#include <stdio.h>

#ifdef _DEBUG
//#define MyDbg(FILE_NAME, STR, ...) MyDebug::Log(FILE_NAME, STR, __VA_ARGS__)
#define WITHOUT_DIR(x) strrchr(x, '\\') ? strrchr(x, '\\') + 1 : x

#define MyDbg(FILE_NAME, STR, ...)					\
	fprintf(stdout, "[%s]", WITHOUT_DIR(FILE_NAME));	\
	fprintf(stdout, STR, __VA_ARGS__);				\
	printf("\n")
#else
#define MyDbg(STR)
#endif

class MyDebug {
public:
	static void Log(char *filename, char *str, char *args) {
		char *fileNameWithoutDir = strrchr(filename, '\\') ? strrchr(filename, '\\') + 1 : filename;
		//std::cout << "[" << fileNameWithoutDir << "]" << str << std::endl;
		fprintf(stdout, "[%s]%s", filename, str, args);
	}
};