import bpy

spawner_list = []
curves_list = []

bpy.types.Object.num_entities = bpy.props.IntProperty()
bpy.types.Object.delay_in_spawn = bpy.props.FloatProperty()
bpy.types.Object.entity_speed = bpy.props.FloatProperty()
bpy.types.Object.entity_type = bpy.props.EnumProperty(items=[
    ("ET_ENEMY_RAZOR", "razor", "spawns razor"),
    ("ET_ENEMY_RAUD", "raud", "spawns raud"),
    ("ET_NONE", "No entity", "dummy entity type")
], name = "Entity Type", )
bpy.types.Object.path_type = bpy.props.EnumProperty(items=[
    ("PT_SPLINE", "Spline path", "Path made of bezier splines"),
    ("PT_SINE", "Sine curve", "Sine function"),
    ("PT_SIMPLE", "Simple", "Simple path, object flies straight")
], name = 'Path type')

def ExportBezier(fileHandle, bezCurveObj):
    bezCurve = bpy.data.curves[bezCurveObj.name]
    splineObj = bezCurve.splines[0]
    worldMatrix = bezCurveObj.matrix_world
    controlPoints = splineObj.bezier_points
    
    controlPoint1 = worldMatrix * controlPoints[0].co
    controlPoint2 = worldMatrix * controlPoints[0].handle_right
    controlPoint3 = worldMatrix * controlPoints[1].handle_left
    controlPoint4 = worldMatrix * controlPoints[1].co
    
    fileHandle.write("<control_point ")
    fileHandle.write("position=\"" + str(round(controlPoint1[0],2)) + " " + str(round(controlPoint1[2], 2)) + " " + str(round(-controlPoint1[1],2)) + "\" ")
    fileHandle.write("/>\n")
    
    fileHandle.write("<control_point ")
    fileHandle.write("position=\"" + str(round(controlPoint2[0],2)) + " " + str(round(controlPoint2[2], 2)) + " " + str(round(-controlPoint2[1],2)) + "\" ")
    fileHandle.write("/>\n")
    
    fileHandle.write("<control_point ")
    fileHandle.write("position=\"" + str(round(controlPoint3[0],2)) + " " + str(round(controlPoint3[2], 2)) + " " + str(round(-controlPoint3[1],2)) + "\" ")
    fileHandle.write("/>\n")
    
    fileHandle.write("<control_point ")
    fileHandle.write("position=\"" + str(round(controlPoint4[0],2)) + " " + str(round(controlPoint4[2], 2)) + " " + str(round(-controlPoint4[1],2)) + "\" ")
    fileHandle.write("/>\n")

class ExportLevel(bpy.types.Operator):
    bl_idname = "project_shmup2.export_level"
    bl_label = "Save level to file"
    
    def execute(self, context):
        targetFile = open('E:\\Level_1.xml', 'w')
        targetFile.write("<level>\n")
        targetFile.write("<spawners>\n")
        for each_sp in spawner_list:
            targetFile.write("<spawner ")
            targetFile.write("position=\"")
            targetFile.write(str(round(each_sp.location[0], 2)) + " " + str(round(each_sp.location[2],2)) + "\" ")
            targetFile.write("num_entities=\"" + str(each_sp.num_entities) + "\" ")
            targetFile.write("delay=\"" + str(round(each_sp.delay_in_spawn, 2)) + "\" ")
            targetFile.write("entity_type=\"" + each_sp.entity_type + "\" ")
            targetFile.write("path_type=\"" + each_sp.path_type + "\" ")
            targetFile.write(">")
            for each_child in each_sp.children:
                print("writing child " + each_child.name)
                targetFile.write("\n")
                #bezierCurve = bpy.data.curves[each_child.name]
                ExportBezier(targetFile, each_child)
            targetFile.write("</spawner>")
            targetFile.write("\n")
        targetFile.write("</spawners>\n")
        targetFile.write("</level>\n")
        targetFile.close()
        return {'FINISHED'}

class AddSpawner(bpy.types.Operator):
    bl_idname = "project_shmup2.add_spawner"
    bl_label = "Spawner"
    
    def execute(self, context):
        bpy.ops.mesh.primitive_cube_add(location=(0.0, 0.0, 0.0))
        ob = context.active_object
        ob.scale = (8.0, 8.0, 8.0)    
        ob.num_entities = 0
        ob.delay_in_spawn = 0.0
        ob.entity_speed = 0.0
        ob.entity_type = "ET_NONE"
        ob.path_type = "PT_SIMPLE"
        ob.name = "spawner." + str(len(spawner_list) + 1)
        ob.data.name = ob.name
        spawner_list.append(ob)
        
        return {'FINISHED'}

class RemoveSpawner(bpy.types.Operator):
    bl_idname = "project_shmup2.remove_spawner"
    bl_label = "Delete"
    ob_name = bpy.props.StringProperty()
    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        ob = context.scene.objects[self.ob_name]
        #context.scene.objects.active = ob       
        for each_child in ob.children:
            #print("removing child " + each_child)
            each_child.select = True
            curv = each_child.data
            bpy.ops.Object.delete()
            #print("removing curve " + curv)
            bpy.data.curves.remove(curv)
        meshObj = ob.data
        ob.select = True
        bpy.ops.Object.delete()
        bpy.data.meshes.remove(meshObj)
        spawner_list.remove(ob)
        return {'FINISHED'}
        
class RemoveAllSpawners(bpy.types.Operator):
    bl_idname = "project_shmup2.remove_all_spawners"
    bl_label = "Remove all"
    
    def execute(self, context):
        if len(spawner_list) > 0:
            oldmode = bpy.context.mode
            #bpy.ops.object.mode_set(mode='OBJECT')
            for each_sp in spawner_list:
                for each_child in each_sp.children:
                    each_child.select = True;
                    bpy.ops.Object.delete()
                each_sp_mesh = each_sp.data
                each_sp.select = True;
                bpy.ops.Object.delete()
                bpy.data.meshes.remove(each_sp_mesh)
            for each_curv in bpy.data.curves:
                bpy.data.curves.remove(each_curv)
            del spawner_list[:]
            #bpy.ops.object.mode_set(mode=oldmode)
        return {'FINISHED'}

class SelectSpawner(bpy.types.Operator):
    bl_idname = "project_shmup2.select_spawner"
    bl_label = "Select"
    ob_name = bpy.props.StringProperty()
    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        ob = context.scene.objects[self.ob_name]
        ob.select = True
        context.scene.objects.active = ob
        return {'FINISHED'}
    
class AddCurve(bpy.types.Operator):
    bl_idname = "project_shmup2.add_bezier_curve"
    bl_label = "New curve"
    
    def execute(self, context):
        selectedSpawner = context.active_object
        selectedSpawner.path_type = "PT_SPLINE"
        bpy.ops.curve.primitive_bezier_curve_add(location=(0.0,0.0,0.0), radius=20.0, rotation=(3.141 * 0.5,0.0,3.141))
        newCurve = context.active_object
        newCurve.parent = selectedSpawner
        return {'FINISHED'}        

class RemoveCurve(bpy.types.Operator):
    bl_idname = "project_shmup2.remove_curve"
    bl_label = "Remove curve"
    
    def execute(self, context):
        oldmode = context.mode
        bpy.ops.object.mode_set(mode='OBJECT')
        for each_sp in spawner_list:
            each_sp.select = True;            
            bpy.ops.Object.delete()
            return {'FINISHED'}
        bpy.ops.object.mode_set(mode=oldmode)        

        
class ShmupEditorPanel(bpy.types.Panel):
    bl_idname = "project_shmup2.editor_panel"
    bl_label = "Shmup Level Editor"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'SHMUP_EDITOR'
    
    def draw(self, context):
        layout = self.layout
        currObj = context.active_object
        currScene = bpy.context.scene     
        
        row = layout.row()
        row.operator("project_shmup2.export_level", text="Save level to file")
        row = layout.row()
        row.operator("mesh.primitive_cube_add", text="load level from file")
        
        row = layout.row()
        row.operator("project_shmup2.add_spawner")
        row.operator("project_shmup2.remove_all_spawners")
        #row = layout.row()
        #row.operator("project_shmup2.add_bezier_curve")
        #row.operator("project_shmup2.remove_all_curves")
        for each_sp in spawner_list: 
            col = layout.column(align=True)
            split = col.split(align=True, percentage=0.3)
            split.label(text = each_sp.name)
            split.operator("project_shmup2.select_spawner").ob_name = each_sp.name
            split.operator("project_shmup2.remove_spawner").ob_name = each_sp.name
            row = col.row(align=True)
            row.prop(each_sp, "location")
            #row = col.row(align=True)
            #row.prop(each_sp, "num_entities")
            #row = col.row(align=True)
            #row.prop(each_sp, "delay_in_spawn")
            #row = col.row(align=True)
            #split = row.split(align=True, percentage=0.5)
            #split.label(text="entity type")
            #split.prop(each_sp, "entity_type", text="")
            col.row().label(text = "--------------------------------------------------------------------------------------------------")

class SpawnerEditorPanel(bpy.types.Panel):
    bl_idname = "project_shmup2.spawner_editor_panel"
    bl_label = "Spawner Editor"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    
    def draw(self, context):
        layout = self.layout
        currObj = context.active_object
        currScene = bpy.context.scene
        if currObj and currObj.select == True:
            col = layout.column(align=True)
            row = col.row(align=True)
            row.label(text = currObj.name)
            row = col.row(align=True)
            row.prop(currObj, "num_entities")
            row = col.row(align=True)
            row.prop(currObj, "delay_in_spawn")
            row = col.row(align=True)
            split = row.split(align=True, percentage=0.5)
            split.label(text="entity type")
            split.prop(currObj, "entity_type", text="")
            row = col.row(align=True)
            split = row.split(align=True, percentage=0.5)
            split.label(text="Path type")
            split.prop(currObj, "path_type", text="")
            row = col.row(align=True)
            row.operator("project_shmup2.add_bezier_curve")
            row.operator("project_shmup2.remove_curve")
            
    
def register():
    bpy.utils.register_class(AddSpawner)
    bpy.utils.register_class(RemoveSpawner)
    bpy.utils.register_class(RemoveAllSpawners)
    bpy.utils.register_class(SelectSpawner)
    bpy.utils.register_class(AddCurve)
    bpy.utils.register_class(RemoveCurve)
    bpy.utils.register_class(ShmupEditorPanel)
    bpy.utils.register_class(ExportLevel)
    bpy.utils.register_class(SpawnerEditorPanel)

def unregister():
    bpy.utils.unregister_class(AddSpawner)
    bpy.utils.unregister_class(RemoveSpawner)    
    bpy.utils.unregister_class(RemoveAllSpawners)
    bpy.utils.unregister_class(SelectSpawner)
    bpy.utils.unregister_class(AddCurve)
    bpy.utils.unregister_class(RemoveCurve)
    bpy.utils.unregister_class(ShmupEditorPanel)
    bpy.utils.unregister_class(ExportLevel)
    bpy.utils.unregister_class(SpawnerEditorPanel)
    
if __name__ == "__main__":
    register()