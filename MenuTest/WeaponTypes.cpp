#include"WeaponTypes.h"
#include"EntityUtil.h"

void ShootRapidLaser(EntityUtil* entUtl, Ogre::Vector3 pos) {
	entUtl->CreateEntityFromTemplate(EntityType::ET_STANDARD_BULLET, pos);
}

void ShootSpread(EntityUtil* entUtl, Ogre::Vector3 pos) {
	int totalPellets = 5;
	int lowerBound = totalPellets / 2;
	float stepAngle = 5.0f;
	for (int i = 0; i < totalPellets; i++) {
		Ogre::Quaternion orientation(Ogre::Degree(stepAngle) * (-(float)lowerBound + i), Ogre::Vector3::UNIT_Z);
		//entUtl->CreateEntityFromType(ET_SPREAD_BULLET, pos, orientation);
		entUtl->CreateEntityFromTemplate(EntityType::ET_SPREAD_BULLET, pos, orientation);
	}
}

void ShootHomingMissile(EntityUtil* entUtl, Ogre::Vector3 pos) {

}

void ShootLaser(EntityUtil* entUtl, Ogre::Vector3 pos) {

}