//#include "Component.h"
#include "EntityUtil.h"
#include "Dbg.h"
#include "WeaponTypes.h"

typedef void (*ShootWeapon)(EntityUtil*, Ogre::Vector3);

class Weapon {
public:
	Weapon();
	Weapon(EntityUtil *entityCreator);
	void SetEntityCreator(EntityUtil *);
	void InitAllWeaponTypes();
	void Shoot(Ogre::Vector3, float);
	void CreateProjectile();
	void CreateProjectile(Ogre::Vector3 startPos);
	void ResetShootingDelay();
	void SetCurrentWeapon(WeaponType);
	
private:
	EntityUtil*		mEntityCreator;
	float			mDelayCounter;
	WeaponType		mCurrentWeaponType;

	struct WeaponAttributes {
		float		mShootingDelay;
		BulletType  mBulletType;
		ShootWeapon Shoot;
	} mWeaponAttr[WeaponType::WT_LAST];
};