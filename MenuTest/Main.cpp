#include "vld.h"
#include "Graphics.h"
#include "StateManager.h"
#include "MenuState.h" 
#include "GameState.h"
#include "Input.h"
#include "Dbg.h"

int main(int argc, char *argv[]) {
	InputHandler *inputHandler = new InputHandler();
	StateManager *stateManager = new StateManager();
	Graphics *graphics = new Graphics();
	stateManager->SetInputHandler(inputHandler);
	stateManager->SetGraphicsSystem(graphics);
	MyDbg(__FILE__, "Calling graphics::init()");
	if (!graphics->Init(800, 600)) {
		MyDbg(__FILE__, "Error in graphics::init()");
		delete stateManager;
		delete graphics;
		delete inputHandler;
		//graphics->ShutDown();
		return 1;
	}

	MenuState::CreateState(StateId::MAIN_MENU, stateManager);
	GameState::CreateState(StateId::IN_GAME, stateManager);
	
	stateManager->PushState(StateId::MAIN_MENU);

	LARGE_INTEGER ticksPerSec;
	LARGE_INTEGER	currTickCounter, 
					prevTickCounter;
	QueryPerformanceFrequency(&ticksPerSec);
	double ticksPerMSec = ticksPerSec.QuadPart / 1000.0f;
	float deltaTime;
	
	QueryPerformanceCounter(&prevTickCounter);
	while (stateManager->mIsRunning) {		
		QueryPerformanceCounter(&currTickCounter);
		deltaTime = (currTickCounter.QuadPart - prevTickCounter.QuadPart) / (float)ticksPerSec.QuadPart;
		prevTickCounter = currTickCounter;
		//MyDbg(__FILE__, "deltaTime: %f", deltaTime);
		while (SDL_PollEvent(&graphics->sdlEvent)) {
			if (!inputHandler->CaptureEvent(graphics->sdlEvent))
				stateManager->mIsRunning = false;
		}
		stateManager->Update(deltaTime);
		graphics->Update();

	}
	
	delete stateManager;
	graphics->ShutDown();
	delete graphics;
	delete inputHandler;


	return 0;
}