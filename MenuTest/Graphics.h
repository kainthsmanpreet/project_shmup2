#ifndef GRAPHICS_H
#define GRAPHICS_H

#include<Ogre.h>
#include<SDL.h>
#include<SDL_syswm.h>
#include<SDL_opengl.h>

#include<MYGUI\MyGUI.h>
#include<MYGUI\MyGUI_OgrePlatform.h>

class Graphics {
	SDL_SysWMinfo sdlSysWmInfo;
	SDL_Window *sdlWindow;

	Ogre::String windowHandle;
	Ogre::NameValuePairList paramList;
	
	int particleSysCounter;

public:
	Ogre::Root *ogreRoot;
	Ogre::SceneManager *ogreSceneManager;
	Ogre::RenderWindow *ogreRenderWindow;
	Ogre::Viewport *ogreViewPort;
	Ogre::Camera	*mainCamera;

	MyGUI::Gui *gui;
	MyGUI::OgrePlatform *ogreGUIPlatform;

	Ogre::Vector3	worldTopLeft,
					worldBottomRight;

	SDL_Event sdlEvent;
	bool Init(int, int);
	void ShutDown();
	void Update();

	void SetCameraPosition(Ogre::Vector3);
	void SetBackgroundColour(Ogre::ColourValue);
	Ogre::SceneNode* AddSceneNode(Ogre::Vector3 position);
	Ogre::SceneNode* AddSceneNode(const Ogre::String &);
	Ogre::SceneNode* AddSceneNode();
	Ogre::Entity* AddEntity(const Ogre::String &, const Ogre::String &);
	Ogre::Entity* AddEntity(const Ogre::String &);
	Ogre::Entity* AddEntity(const Ogre::MeshPtr &);
	Ogre::ParticleSystem* AddParticleSystem(const Ogre::String &);
	MyGUI::Gui* GetGUIPtr();
};


#endif