#include"MenuState.h"

MenuState::MenuState() {
	MyDbg(__FILE__, "Menustate Constructor");
}

MenuState::~MenuState() {
	MyDbg(__FILE__, "Menustate destructor");
}

void MenuState::EnterState() {
	MyDbg(__FILE__, "Entering menu state");
	
	/*
	graphics->ogreSceneManager = graphics->ogreRoot->createSceneManager(Ogre::ST_GENERIC, "MenuSceneManager");
	graphics->mainCamera = graphics->ogreSceneManager->createCamera("MenuCamera");
	graphics->ogreViewPort->setCamera(graphics->mainCamera);

	graphics->ogreGUIPlatform->initialise(graphics->ogreRenderWindow, graphics->ogreSceneManager);
	graphics->gui->initialise();
	*/
	MyGUI::PointerManager::getInstance().setVisible(true);	
	mWidgets = MyGUI::LayoutManager::getInstance().loadLayout("test.layout");
	
	MyGUI::WidgetPtr rootPanel = mWidgets[0];
	
	for (size_t i = 0; i < rootPanel->getChildCount(); i++) {
		MyGUI::ButtonPtr someButton = static_cast<MyGUI::ButtonPtr>(rootPanel->getChildAt(i));
		someButton->eventMouseButtonClick += MyGUI::newDelegate(this, &MenuState::OnMouseClick);
	}
	timer = 0;
}

void MenuState::EnterStateWithHandler(InputHandler *inputHandler) {
	MyDbg(__FILE__, "Entering menu state");
	inputHandler->SetListener(this);
	timer = 0;
}

void MenuState::ExitState() {
	MyDbg(__FILE__, "Exiting menu state");
	MyGUI::LayoutManager::getInstance().unloadLayout(mWidgets);
	
	/*
	graphics->gui->shutdown();
	graphics->ogreGUIPlatform->shutdown();

	graphics->ogreSceneManager->destroyCamera(graphics->mainCamera);
	graphics->ogreRoot->destroySceneManager(graphics->ogreSceneManager);
	*/

}

void MenuState::Update() {

}

void MenuState::Update(float dt) {

}

void MenuState::OnKeyDown(const InputState &keyData) {
	if (keyData.KeyDownAction[ACTION_UP])
		MyDbg(__FILE__, "Action up pressed");

	if (keyData.KeyDownAction[ACTION_DOWN])
		MyDbg(__FILE__, "action down pressed");

	if (keyData.KeyDownAction[ACTION_PREVIOUS])
		this->PopState();
}

void MenuState::OnMouseClick(MyGUI::WidgetPtr senderWidget) {
	MyDbg(__FILE__, "%s clicked", senderWidget->getName().c_str());
	MyDbg(__FILE__, "width: %d", graphics->ogreRenderWindow->getWidth());
	MyDbg(__FILE__, "height: %d", graphics->ogreRenderWindow->getHeight());


	if (senderWidget->getName() == "Btn_NewGame")
		this->SetState(StateId::IN_GAME); 

	else if (senderWidget->getName() == "Btn_Exit")
		this->PopState();
}