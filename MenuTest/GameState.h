#include "State.h"
#include "Component.h"
#include "System.h"
#include "Artemis\Artemis.h"
#include "btBulletCollisionCommon.h"
#include "DebugDraw.hpp"
#include "EntityUtil.h"
#include "LevelUtil.h"
#include "Player.h"
#include "Dbg.h"

class GameState : public State {
private:
	GameState();
	void InitScene();
	void InitPhysics();
	void ProcessInput();
	void SetupScreenBounds();
	void ProcessCollisions();
	void SetupBackground();

	MyGUI::VectorWidgetPtr	mWidgets;

	Ogre::SceneNode			*backgroundNode;
	Ogre::Rectangle2D		*backgroundRect;
	Ogre::ParticleSystem	*backgroundParticleFx;

	Ogre::Vector3			cameraPos;
	Ogre::Vector3			topLeft, 
							bottomRight;

	btCollisionWorld		*mCollisionWorld;
	btCollisionConfiguration *mCollisionConfig;
	btCollisionDispatcher	*mCollisionDispatcher;
	btBroadphaseInterface	*mBroadphaseInterface;
	CDebugDraw				*mBtDbgDraw;

	btCollisionShape		*mScreenBoundShape;

	artemis::World			*mWorld;
	artemis::EntityManager	*mEntityMgr;
	artemis::SystemManager	*mSysMgr;

	EntityUtil				*mEntityUtil;
	LevelUtil				*mLevelUtil;

	PS_System::TransformUpdate	*mSysTransform;
	PS_System::RenderUpdate	*mSysRender;
	PS_System::ParticleSystemUpdate	*mSysParticle;
	PS_System::PhysicsUpdate *mSysCollider;
	PS_System::CheckHealth	*mSysHealth;
	PS_System::ExpiryUpdate	*mSysExpire;
	PS_System::SpawnerUpdate *mSysSpawner;
	PS_System::SplineUpdate *mSysSpline;

	artemis::Entity			*mFoo;
	PS_Component::Transform *mFooTransform;

	Player					*player;
	bool					physxDbg;

public:
	~GameState();

	DECLARE_SINGLETON(GameState);

	void EnterState();
	void ExitState();
	virtual void Update();
	virtual void Update(float dt);
	void OnKeyDown(const InputState &);
	void OnKeyUp(const InputState &);
	void OnMouseClick(MyGUI::WidgetPtr);
	void OnMouseDown(const InputState &);
	void OnMouseUp(const InputState &);
	void LoadLevel();
};