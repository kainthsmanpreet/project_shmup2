#pragma once
#include<string>
#include"Graphics.h"
#include"Component.h"
#include"Artemis\World.h"
#include"Artemis\Entity.h"
#include"Dbg.h"
#include"btBulletCollisionCommon.h"
#include "CollisionTags.h"
#include "CollisionEvent.h"
#include "EntityTypes.h"
#include "EntityPrototype.h"
#include "tinyxml2.h"

using namespace PS_Component;
using namespace tinyxml2;

struct SpawnerInfo {
	int numEntities;
	float delayInSpawn;
	EntityType entityType;
	float speed;
	Component *compToAttach;
};

class EntityUtil {
public:
	EntityUtil(	artemis::World *world, 
				Graphics *graphics, 
				btCollisionWorld *colWorld) :
		mEntityWorld(world),
		mGraphics(graphics),
		mColWorld(colWorld) 
	{
		for (int i = 0; i < ET_LAST; i++) {
			mCollisionShapeCache[i] = NULL;
		}
		InitPrototypes();
	}

	~EntityUtil();
	void				CreateEntityFromName(char *, Ogre::Vector3);
	void				CreateEntityFromType(EntityType,	const Ogre::Vector3 &pos,
															const Ogre::Quaternion &orient = Ogre::Quaternion::IDENTITY, 
															const Ogre::Vector3 &scale = Ogre::Vector3::UNIT_SCALE);
	artemis::Entity*	CreateEntityFromTemplate(EntityType,const Ogre::Vector3 &pos,
															const Ogre::Quaternion &orient = Ogre::Quaternion::IDENTITY, 
															const Ogre::Vector3 &scale = Ogre::Vector3::UNIT_SCALE);
	void				CreateSpawnerEntity(Ogre::Vector3, SpawnerInfo &);
	void				CreateSpawnerEntity(Ogre::Vector3, int, float, EntityType, float);
	void				DestroyEntity(Entity*);
	void				InitPrototypes();
private:
	EntityUtil() {}
	btCollisionObject*	CreateCollisionObject(btBoxShape *, const btVector3&, int group, int mask);
	btCollisionObject*	CreateCollisionObject(btBoxShape *, const btVector3&, const btQuaternion&, int group, int mask);

	void				CheckShapeCache(EntityType, const btVector3&);
	void				InitComponentStrMapping();
	void				InitEntityTypeMapping();
	void				ParseEntityXML(const char *fileName);

	artemis::World		*mEntityWorld;
	Graphics			*mGraphics;
	btCollisionWorld	*mColWorld;
	btBoxShape			*mCollisionShapeCache[EntityType::ET_LAST];
	EntityPrototype		mEntityTemplates[EntityType::ET_LAST];
	XMLDocument			xmlDoc;

	std::unordered_map<std::string, DeSerializeDelegate>	componentDeSerMap;
};
