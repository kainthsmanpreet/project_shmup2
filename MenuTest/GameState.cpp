#include"GameState.h"

GameState::GameState() {
	MyDbg(__FILE__, "Gamestate constructor");
}

GameState::~GameState() {
	MyDbg(__FILE__, "GameState destructor");
}

void GameState::EnterState() {
	MyDbg(__FILE__, "Entering game state");
	gui = this->graphics->GetGUIPtr();
	mWidgets = MyGUI::LayoutManager::getInstance().loadLayout("ingame.layout");
	graphics->SetBackgroundColour(Ogre::ColourValue(0.4f, 0.4f, 0.7f, 1.0f));
	//graphics->mainCamera->setPosition(new Ogre::Vector3());
	InitPhysics();
	InitScene();
}

void GameState::InitScene() {
	physxDbg = 0;
	//Ogre::Vector3 cameraPos(0.0f, 0.0f, 120.0f);
	cameraPos = Ogre::Vector3(0.0f, 0.0f, 150.0f);
	graphics->mainCamera->setPosition(cameraPos);
	graphics->mainCamera->lookAt(Ogre::Vector3::ZERO);
	graphics->mainCamera->setFOVy(Ogre::Radian(Ogre::Math::PI * 0.34f));
	graphics->mainCamera->setNearClipDistance(0.3f);
	graphics->mainCamera->setFarClipDistance(1500.0f);
	graphics->ogreSceneManager->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));

	Ogre::Light *light = graphics->ogreSceneManager->createLight("mainLight");
	light->setDirection(Ogre::Vector3(0.0f, -1.0f, -1.0f));
	light->setType(Ogre::Light::LT_DIRECTIONAL);

	MyDbg(__FILE__, "graphics->mainCamera->getFOVy():  %f", graphics->mainCamera->getFOVy().valueDegrees());
	MyDbg(__FILE__, "graphics->mainCamera->getAspectRatio() : %f", graphics->mainCamera->getAspectRatio());

	float x, y;
	y = cameraPos.z * Ogre::Math::Tan(graphics->mainCamera->getFOVy() * 0.5f);
	x = cameraPos.z * Ogre::Math::Tan(graphics->mainCamera->getFOVy() * 0.5f) * graphics->mainCamera->getAspectRatio();

	topLeft.x = -x;
	topLeft.y = y;

	bottomRight.x = x;
	bottomRight.y = -y;

	MyDbg(__FILE__, "screen bounds x : %f, y : %f", x, y);
	MyDbg(__FILE__, "render window width: %d, heigth: %d", graphics->ogreViewPort->getActualWidth(), graphics->ogreViewPort->getActualHeight());

	graphics->worldTopLeft = topLeft;
	graphics->worldBottomRight = bottomRight;

	mWorld = new artemis::World();
	mWorld->setDelta(1.0f / 60.0f);
	mEntityMgr = mWorld->getEntityManager();
	mSysMgr = mWorld->getSystemManager();

	mEntityUtil = new EntityUtil(mWorld, graphics, mCollisionWorld);
	mLevelUtil = new LevelUtil(mEntityUtil);

	mSysTransform = (PS_System::TransformUpdate *)mSysMgr->setSystem(new PS_System::TransformUpdate());
	mSysRender = (PS_System::RenderUpdate *)mSysMgr->setSystem(new PS_System::RenderUpdate());
	mSysParticle = (PS_System::ParticleSystemUpdate *)mSysMgr->setSystem(new PS_System::ParticleSystemUpdate());
	mSysCollider = (PS_System::PhysicsUpdate *)mSysMgr->setSystem(new PS_System::PhysicsUpdate());
	mSysHealth = (PS_System::CheckHealth *)mSysMgr->setSystem(new PS_System::CheckHealth(mEntityUtil));
	mSysExpire = (PS_System::ExpiryUpdate *)mSysMgr->setSystem(new PS_System::ExpiryUpdate());
	mSysSpawner = (PS_System::SpawnerUpdate *)mSysMgr->setSystem(new PS_System::SpawnerUpdate(mEntityUtil));
	mSysSpline = (PS_System::SplineUpdate *)mSysMgr->setSystem(new PS_System::SplineUpdate());

	mSysMgr->initializeAll();

	player = new Player();
	player->init(mWorld, graphics, mCollisionWorld, mEntityUtil);
	player->SetScreenBounds(topLeft, bottomRight);

	SetupScreenBounds();
	//mInputHandler->AddListener(player);
	graphics->mainCamera->setPosition(0.0f, 0.0f, 400.0f);

	SetupBackground();
	//LoadLevel();
	mLevelUtil->LoadLevelFromXML(1);
	//mEntityUtil->CreateEntityFromTemplate(EntityType::ET_ENEMY_RAZOR, Ogre::Vector3::ZERO);
}

void GameState::InitPhysics() {
	mCollisionConfig = new btDefaultCollisionConfiguration();
	mCollisionDispatcher = new btCollisionDispatcher(mCollisionConfig);
	mBroadphaseInterface = new btDbvtBroadphase();
	mCollisionWorld = new btCollisionWorld(
		mCollisionDispatcher,
		mBroadphaseInterface,
		mCollisionConfig
		);
	mBtDbgDraw = new CDebugDraw(graphics->ogreSceneManager, mCollisionWorld);
}

void GameState::ExitState() {
	MyDbg(__FILE__, "Exiting game state");

	MyGUI::LayoutManager::getInstance().unloadLayout(mWidgets);
	delete mWorld;

	//btCollisionObjectArray colObjArr = mCollisionWorld->getCollisionObjectArray();
	//for (int i = 0; i < colObjArr.size(); i++) {
	//	mCollisionWorld->removeCollisionObject(colObjArr[i]);
	//	delete colObjArr[i];
	//}

	delete mScreenBoundShape;

	delete mLevelUtil;
	delete mEntityUtil;
	delete player;
	delete mBtDbgDraw;
	delete mCollisionWorld;
	delete mBroadphaseInterface;
	delete mCollisionDispatcher;
	delete mCollisionConfig;

	graphics->ogreSceneManager->clearScene();

}

void GameState::Update() {
	mWorld->loopStart();

	player->ProcessInput(mInputHandler->GetInputState());
	//ProcessInput();

	mSysTransform->process();
	mSysSpline->process();
	player->SetWithinBounds();
	mSysCollider->process();

	mCollisionWorld->performDiscreteCollisionDetection();
	ProcessCollisions();

	mSysSpawner->process();
	mSysHealth->process();
	mSysExpire->process();
	mSysRender->process();
	mSysParticle->process();
	if (physxDbg)
		mBtDbgDraw->Update();
}

void GameState::Update(float dt) {
	mWorld->setDelta(dt);
	mWorld->loopStart();

	player->ProcessInput(mInputHandler->GetInputState());
	//ProcessInput();

	mSysTransform->process();
	mSysSpline->process();
	player->SetWithinBounds();
	mSysCollider->process();

	mCollisionWorld->performDiscreteCollisionDetection();
	ProcessCollisions();

	mSysSpawner->process();
	mSysHealth->process();
	mSysExpire->process();
	mSysRender->process();
	mSysParticle->process();
	if (physxDbg)
		mBtDbgDraw->Update();
}

void GameState::OnKeyDown(const InputState &keyData) {
	//MyDbg(__FILE__, "OnKeyDown");
	float speed = 60.0f;
	if (keyData.KeyDownAction[ACTION_PREVIOUS]) {
		SetState(StateId::MAIN_MENU);
	}

	if (keyData.KeyDownAction[ACTION_PHYSX_DBG]) {
		physxDbg = !physxDbg;
	}

	/*
	if (keyData.KeyPressed[ACTION_RIGHT]) {
	MyDbg(__FILE__, "action -> right");
	}

	if (keyData.KeyPressed[ACTION_LEFT]) {
	MyDbg(__FILE__, "action -> left");
	}

	if (keyData.KeyPressed[ACTION_UP]) {
	MyDbg(__FILE__, "action -> up");
	}

	if (keyData.KeyPressed[ACTION_DOWN]) {
	MyDbg(__FILE__, "action -> down");
	}
	*/
}

void GameState::OnKeyUp(const InputState &keyData) {
	//MyDbg(__FILE__, "on key up");
}

void GameState::OnMouseDown(const InputState &keyData) {
	MyDbg(__FILE__, "on mouse down");
}

void GameState::OnMouseUp(const InputState &keyData) {
	MyDbg(__FILE__, "on mouse up");
}

void GameState::ProcessInput() {

}

void GameState::SetupScreenBounds() {
	btTransform planeTransform;
	planeTransform.setIdentity();

	MyDbg(__FILE__, "bottomRight.x - topLeft.x : %f", bottomRight.x - topLeft.x);
	mScreenBoundShape = new btBoxShape(btVector3(bottomRight.x, 0.0f, 10.0f));

	mEntityUtil->CreateEntityFromType(ET_BOUNDARY, Ogre::Vector3(bottomRight.x + 10.0f, 0.0f, 0.0f), Ogre::Quaternion(Ogre::Degree(90.0f), Ogre::Vector3::UNIT_Z));
	mEntityUtil->CreateEntityFromType(ET_BOUNDARY, Ogre::Vector3(0.0f, topLeft.y + 10.0f, 0.0f));
	mEntityUtil->CreateEntityFromType(ET_BOUNDARY, Ogre::Vector3(topLeft.x - 10.0f, 0.0f, 0.0f), Ogre::Quaternion(Ogre::Degree(90.0f), Ogre::Vector3::UNIT_Z));
	mEntityUtil->CreateEntityFromType(ET_BOUNDARY, Ogre::Vector3(0.0f, bottomRight.y - 10.0f, 0.0f));

	/*
	for (int i = 0; i < 4; i++) {
	mScreenBounds[i] = new btCollisionObject();
	mScreenBounds[i]->setCollisionShape(mScreenBoundShape);
	mCollisionWorld->addCollisionObject(mScreenBounds[i]);
	}

	btQuaternion q(btVector3(0.0f, 0.0f, 1.0f), Ogre::Math::PI * 0.5f);
	planeTransform.setOrigin(btVector3(bottomRight.x, 0.0f, 0.0f));
	planeTransform.setRotation(btQuaternion(btVector3(0.0f, 0.0f, 1.0f), Ogre::Math::PI * 0.5f));
	//mScreenBounds[0]->setWorldTransform(planeTransform);

	planeTransform.setIdentity();
	planeTransform.setOrigin(btVector3(0.0f, topLeft.y, 0.0f));
	//mScreenBounds[1]->setWorldTransform(planeTransform);

	planeTransform.setOrigin(btVector3(topLeft.x, 0.0f, 0.0f));
	planeTransform.setRotation(btQuaternion(btVector3(0.0f, 0.0f, 1.0f), Ogre::Math::PI * 0.5f));
	//mScreenBounds[2]->setWorldTransform(planeTransform);

	planeTransform.setIdentity();
	planeTransform.setOrigin(btVector3(0.0f, bottomRight.y, 0.0f));
	//mScreenBounds[3]->setWorldTransform(planeTransform);
	*/
}

void GameState::ProcessCollisions() {
	int numCollisions = mCollisionDispatcher->getNumManifolds();

	for (int i = 0; i < numCollisions; i++) {
		btPersistentManifold *collidedPair = mCollisionWorld->getDispatcher()->getManifoldByIndexInternal(i);
		Collider *collider1 = static_cast<Collider*>(collidedPair->getBody0()->getUserPointer());
		Collider *collider2 = static_cast<Collider*>(collidedPair->getBody1()->getUserPointer());

		/*eventBody1->OnCollision(eventBody2);
		eventBody2->OnCollision(eventBody1);*/
		collider1->OnCollision(collider1->mEntity, collider2->mEntity);
		collider2->OnCollision(collider2->mEntity, collider1->mEntity);
	}
}

void GameState::LoadLevel() {
	mEntityUtil->CreateSpawnerEntity(Ogre::Vector3(150.0f, 20.0f, 0.0f), 4, 0.2f, ET_ENEMY_RAZOR, 14.0f);
	mEntityUtil->CreateSpawnerEntity(Ogre::Vector3(180.0f, 0.0f, 0.0f), 5, 0.2f, ET_ENEMY_RAZOR, 10.0f);
	mEntityUtil->CreateSpawnerEntity(Ogre::Vector3(200.0f, 40.0f, 0.0f), 10, 0.2f, ET_ENEMY_RAUD, 60.0f);
	mEntityUtil->CreateSpawnerEntity(Ogre::Vector3(240.0f, -40.0f, 0.0f), 10, 0.2f, ET_ENEMY_RAUD, 60.0f);
	mEntityUtil->CreateSpawnerEntity(Ogre::Vector3(350.0f, -50.0f, 0.0f), 4, 0.2f, ET_ENEMY_RAZOR, 24.0f);
}

void GameState::SetupBackground() {
	Ogre::AxisAlignedBox aabb;
	aabb.setInfinite();

	backgroundRect = new Ogre::Rectangle2D(true);
	backgroundRect->setCorners(-1.0f, 1.0f, 1.0f, -1.0f);
	backgroundRect->setBoundingBox(aabb);

	backgroundNode = graphics->AddSceneNode("background_node");
	backgroundRect->setMaterial("Background");
	backgroundRect->setRenderQueueGroup(Ogre::RenderQueueGroupID::RENDER_QUEUE_BACKGROUND);
	backgroundNode->attachObject(backgroundRect);

	backgroundParticleFx = graphics->AddParticleSystem("starfield");
	
	for (int i = 0; i < backgroundParticleFx->getNumEmitters(); i++) {
		Ogre::Vector3 pos;
		float timeToLive;
		pos.x = (cameraPos.z - backgroundParticleFx->getEmitter(i)->getPosition().z) * Ogre::Math::Tan(graphics->mainCamera->getFOVy() * 0.5f) * graphics->mainCamera->getAspectRatio();
		pos.y = 0.0f;
		pos.z = backgroundParticleFx->getEmitter(i)->getPosition().z;
		backgroundParticleFx->getEmitter(i)->setPosition(pos);
		//backgroundParticleFx->getEmitter(i)-> = 2.0f * (cameraPos.z - backgroundParticleFx->getEmitter(i)->getPosition().z) * Ogre::Math::Tan(graphics->mainCamera->getFOVy() * 0.5f);
		timeToLive = (2.0f * pos.x) / (backgroundParticleFx->getEmitter(i)->getMinParticleVelocity());
		backgroundParticleFx->getEmitter(i)->setTimeToLive(timeToLive);
	}
	backgroundNode->attachObject(backgroundParticleFx);
	backgroundParticleFx->fastForward(25.0f);

}