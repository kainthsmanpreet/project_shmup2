#include "Graphics.h"
#include "Component.h"
#include "Input.h"
#include "Artemis\World.h"
#include "Artemis\Entity.h"
#include "BulletCollision\CollisionDispatch\btCollisionWorld.h"
#include "BulletCollision\CollisionShapes\btBoxShape.h"
#include "Weapon.h"
#include "Dbg.h"

class Player {
public:
	Player() {}
	~Player();

	void init(artemis::World*, Graphics*, btCollisionWorld*, EntityUtil *);
	void init(artemis::Entity);
	void OnKeyDown(const InputState&);
	void OnKeyUp(const InputState&);
	void ProcessInput(const InputState&);
	void SetScreenBounds(Ogre::Vector3, Ogre::Vector3);
	void SetWithinBounds();

	PS_Component::Transform		*mTransform;
	PS_Component::Kinematics	*mKinematics;
	btCollisionShape			*mCollisionShape;

private:
	Ogre::Vector3		topLeft,
						bottomRight;
	float				mAccel, mDeAccel;
	float				mMaxSpeed;
	Weapon				mWeapon;
	artemis::World*		mEntWorld;
};