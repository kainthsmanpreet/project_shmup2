#include "Graphics.h"
#include <sstream>

bool Graphics::Init(int scrWidth, int scrHeight) {
	particleSysCounter = 0;
	ogreRoot = new Ogre::Root("../cfg/plugins_d.cfg", "../cfg/ogre.cfg");
	if (!ogreRoot->showConfigDialog())
		return false;

	//ogreRoot->setRenderSystem(ogreRoot->getRenderSystemByName("OpenGL Rendering Subsystem"));
	ogreRenderWindow = ogreRoot->initialise(false);

	SDL_InitSubSystem(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	sdlWindow = SDL_CreateWindow("SDL test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, scrWidth, scrHeight, SDL_WINDOW_OPENGL /*| SDL_WINDOW_FULLSCREEN*/ | SDL_WINDOW_BORDERLESS);
	SDL_GL_CreateContext(sdlWindow);

	SDL_GetVersion(&sdlSysWmInfo.version);
	SDL_GetWindowWMInfo(sdlWindow, &sdlSysWmInfo);
	SDL_ShowCursor(SDL_DISABLE);

	windowHandle = Ogre::StringConverter::toString((unsigned long)sdlSysWmInfo.info.win.window);

	paramList["externalWindowHandle"] = windowHandle;
	paramList["externalGLControl"] = Ogre::String("True");
	paramList["externalGLContext"] = Ogre::StringConverter::toString((unsigned long)SDL_GL_GetCurrentContext());

	ogreRenderWindow = ogreRoot->createRenderWindow("test app", scrWidth, scrHeight, true, &paramList);

	ogreSceneManager = ogreRoot->createSceneManager("OctreeSceneManager", "MainSceneManager");

	mainCamera = ogreSceneManager->createCamera("MainCamera");

	ogreViewPort = ogreRenderWindow->addViewport(mainCamera);

	ogreViewPort->setBackgroundColour(Ogre::ColourValue(0.25f, 0.25f, 0.4f, 0.0f));
	
	ogreRenderWindow->setActive(true);

	Ogre::String secName, typeName, archName;
	Ogre::ConfigFile cf;
	cf.load("../cfg/resources_d.cfg");

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
		}
	}
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	ogreGUIPlatform = new MyGUI::OgrePlatform();
	ogreGUIPlatform->initialise(ogreRenderWindow, ogreSceneManager);

	gui = new MyGUI::Gui();
	gui->initialise();

	SDL_GL_SwapWindow(sdlWindow);

	return true;
}

void Graphics::Update() {
	ogreRoot->renderOneFrame();

	SDL_GL_SwapWindow(sdlWindow);
}

void Graphics::ShutDown() {
	SDL_DestroyWindow(sdlWindow);
	SDL_Quit();
	gui->shutdown();
	ogreGUIPlatform->shutdown();
	delete gui;
	delete ogreGUIPlatform;

	ogreRoot->destroySceneManager(ogreSceneManager);
	delete ogreRoot;
}

void Graphics::SetBackgroundColour(Ogre::ColourValue colorVal) {
	this->ogreViewPort->setBackgroundColour(colorVal);
}

Ogre::SceneNode* Graphics::AddSceneNode(Ogre::Vector3 position) {
	Ogre::SceneNode *node = ogreSceneManager->getRootSceneNode()->createChildSceneNode();
	node->setPosition(position);
	return node;
}

Ogre::SceneNode* Graphics::AddSceneNode(const Ogre::String &nodeName) {
	return ogreSceneManager->getRootSceneNode()->createChildSceneNode(nodeName);
}

Ogre::SceneNode* Graphics::AddSceneNode() {
	return ogreSceneManager->getRootSceneNode()->createChildSceneNode();
}

Ogre::Entity* Graphics::AddEntity(const Ogre::String &entityName, const Ogre::String &meshName) {
	return ogreSceneManager->createEntity(entityName, meshName);
}

Ogre::Entity* Graphics::AddEntity(const Ogre::String &meshName) {
	return ogreSceneManager->createEntity(meshName);
}

Ogre::Entity* Graphics::AddEntity(const Ogre::MeshPtr &meshPtr) {
	return ogreSceneManager->createEntity(meshPtr);
}


Ogre::ParticleSystem* Graphics::AddParticleSystem(const Ogre::String &psName) {
	std::stringstream name;
	name << "particle_system_" << particleSysCounter++;
	//Ogre::String name = "particle_system_" + particleSysCounter++;	
	return ogreSceneManager->createParticleSystem(name.str(), psName);
}

MyGUI::Gui* Graphics::GetGUIPtr() {
	return gui;
}
