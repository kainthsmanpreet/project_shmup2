#pragma once
#include <unordered_map>

#define CTAG_BOUNDARY		1 << 0L
#define CTAG_PLAYER			1 << 1L
#define CTAG_PLAYER_BULLET	1 << 2L
#define CTAG_SPAWNER		1 << 3L
#define CTAG_ENEMY			1 << 4L
#define CTAG_ALL			0xFFFF

extern std::unordered_map<std::string, long> ctagMap;