#include "Weapon.h"

Weapon::Weapon() {
	InitAllWeaponTypes();
	SetCurrentWeapon(WeaponType::WT_RAPID_FIRE);
	mDelayCounter = mWeaponAttr[mCurrentWeaponType].mShootingDelay;
}

Weapon::Weapon(EntityUtil *entityCreator) {
	mEntityCreator = entityCreator;
}

void Weapon::SetEntityCreator(EntityUtil *entityCreator) {
	mEntityCreator = entityCreator;
}

void Weapon::Shoot(Ogre::Vector3 origin, float timeDelta) {
	mDelayCounter -= timeDelta;
	if (mDelayCounter <= 0.0f) {
		//CreateProjectile(origin);
		mWeaponAttr[mCurrentWeaponType].Shoot(mEntityCreator, origin);
		mDelayCounter = mWeaponAttr[mCurrentWeaponType].mShootingDelay;
	}
		
}

void Weapon::CreateProjectile() {
	
}

void Weapon::CreateProjectile(Ogre::Vector3 startPos) {
	mEntityCreator->CreateEntityFromType(ET_STANDARD_BULLET, startPos);
}


void Weapon::ResetShootingDelay() {
	mDelayCounter = 0.0f;
}

void Weapon::SetCurrentWeapon(WeaponType wt) {
	mCurrentWeaponType = wt;

}

void Weapon::InitAllWeaponTypes() {
	mWeaponAttr[WeaponType::WT_RAPID_FIRE].mShootingDelay = 0.08f;
	mWeaponAttr[WeaponType::WT_RAPID_FIRE].mBulletType = BulletType::B_RAPID_LASER;
	mWeaponAttr[WeaponType::WT_RAPID_FIRE].Shoot = ShootRapidLaser;
	
	mWeaponAttr[WeaponType::WT_SPREAD].mShootingDelay = 0.12f;
	mWeaponAttr[WeaponType::WT_SPREAD].mBulletType = BulletType::B_SPREAD;
	mWeaponAttr[WeaponType::WT_SPREAD].Shoot = ShootSpread;

	mWeaponAttr[WeaponType::WT_HOMING].mShootingDelay = 0.22f;
	mWeaponAttr[WeaponType::WT_HOMING].mBulletType = BulletType::B_MISSILE;
	mWeaponAttr[WeaponType::WT_HOMING].Shoot = ShootHomingMissile;

	mWeaponAttr[WeaponType::WT_LASER].mShootingDelay = 0.0f;
	mWeaponAttr[WeaponType::WT_LASER].mBulletType = BulletType::B_LASER;
	mWeaponAttr[WeaponType::WT_LASER].Shoot = ShootLaser;
}