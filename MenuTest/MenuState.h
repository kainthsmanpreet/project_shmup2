#ifndef MENU_STATE_H
#define MENU_STATE_H

#include "State.h"
#include <iostream>
#include"Input.h"
#include "Dbg.h"

class MenuState : public State {
private:
	MenuState();
public:
	~MenuState();

	DECLARE_SINGLETON(MenuState);
	void EnterState();
	void EnterStateWithHandler(InputHandler*);
	void ExitState();
	virtual void Update();
	virtual void Update(float);
	void OnKeyDown(const InputState &);
	void OnMouseClick(MyGUI::WidgetPtr);

private:
	int timer;
	MyGUI::Gui* mpGui;
	MyGUI::VectorWidgetPtr mWidgets;
};

#endif