#ifndef INPUT_H
#define INPUT_H

#include "SDL.h"
#include "SDL_keycode.h"
#include "SDL_events.h"
#include<MYGUI\MyGUI.h>
#include<iostream>
#include"Dbg.h"

enum Action {
	ACTION_NONE,
	ACTION_UP,
	ACTION_DOWN,
	ACTION_LEFT,
	ACTION_RIGHT,
	ACTION_EXECUTE,
	ACTION_PREVIOUS,
	ACTION_SHOOT1,
	ACTION_SHOOT2,
	ACTION_PHYSX_DBG,
	ACTION_LAST_DUMMY
};

struct InputState {
	bool	KeyDownAction[ACTION_LAST_DUMMY];
	bool	KeyUpAction[ACTION_LAST_DUMMY];
	bool	KeyPressed[ACTION_LAST_DUMMY];
};


class InputContext {	
private:
	unsigned int mKeyActionMap[300];
	unsigned int mMouseActionMap[6];

protected:
	InputState mInputState;

	bool		mKeyDownAction[ACTION_LAST_DUMMY];
	bool		mKeyUpAction[ACTION_LAST_DUMMY];
	bool		mPrevAction[ACTION_LAST_DUMMY];

public:
	InputContext();
	~InputContext();

	void SetAction(int, bool);
	void ClearActions();
	bool* GetKeyDownStates();
	bool* GetKeyUpStates();
	const InputState& GetInputState();
	void SetMouseButton(int, int);
};

/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/

class InputEventListener {
public:
	InputEventListener() {

	}

	virtual void OnKeyDown(const InputState &) {

	};

	virtual void OnKeyUp(const InputState &) {
	
	};

	virtual void OnMouseDown(const InputState &) {

	}

	virtual void OnMouseUp(const InputState &) {

	}

	virtual void OnMouseMove(int x, int y, int xRel, int yRel) {

	}
};

/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/

class InputHandler {
private:
	InputContext mInputContext;
	InputEventListener *mpInputListener,
		mDummyListener;
	std::vector<InputEventListener *> mListeners;
	std::map <SDL_Keycode, MyGUI::KeyCode > mMyGUIKeyMap;
	std::map <int, MyGUI::MouseButton> mMyGUIMouseMap;

public:
	int mouseX, mouseY;

	InputHandler();
	~InputHandler();
	bool CaptureEvent(SDL_Event&);
	void SetListener(InputEventListener *);
	void AddListener(InputEventListener *);
	void SetupMyGUIButtonMappings();
	const InputState& GetInputState();
};

#endif