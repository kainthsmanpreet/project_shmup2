#pragma once
#include "EntityUtil.h"

class LevelUtil {
public:
	LevelUtil(EntityUtil *);
	void LoadLevelFromXML(int);
	void FinishCurrentLevel();
private:
	LevelUtil() {}
	int currentLevel;
	EntityUtil *entityUtil;
};