#pragma once

#include"CollisionTags.h"
#include"BulletCollision\CollisionDispatch\btCollisionObject.h"
#include <unordered_map>
#include"Dbg.h"

using namespace artemis;
typedef void(*CollisionDelegate)(Entity *, Entity *);

void BoundaryCollision(Entity *, Entity *);
void DamageCollision(Entity *, Entity *);
void SpawnerCollision(Entity *, Entity *);
void SimpleCollision(Entity *, Entity *);

extern std::unordered_map<std::string, CollisionDelegate> collisionCallBackMap;