#pragma once
#include "EntityTypes.h"
#include "Artemis\Component.h"
#include <vector>

struct EntityPrototype {
	std::vector<artemis::Component*> components;

	EntityPrototype();
};