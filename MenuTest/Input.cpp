#include "Input.h"

InputContext::InputContext() {

	for (int i = 0; i < 300; i++) {
		mKeyActionMap[i] = ACTION_NONE;
	}

	ClearActions();
	
	mKeyActionMap[SDL_SCANCODE_W]		= mKeyActionMap[SDL_SCANCODE_UP]	= ACTION_UP;
	mKeyActionMap[SDL_SCANCODE_S]		= mKeyActionMap[SDL_SCANCODE_DOWN]	= ACTION_DOWN;		
	mKeyActionMap[SDL_SCANCODE_LEFT]	= mKeyActionMap[SDL_SCANCODE_A]		= ACTION_LEFT;
	mKeyActionMap[SDL_SCANCODE_RIGHT]	= mKeyActionMap[SDL_SCANCODE_D]		= ACTION_RIGHT;
	mKeyActionMap[SDL_SCANCODE_LCTRL]	= ACTION_SHOOT1;
	mKeyActionMap[SDL_SCANCODE_LALT]	= ACTION_SHOOT2;
	mKeyActionMap[SDL_SCANCODE_ESCAPE]	= ACTION_PREVIOUS;
	mKeyActionMap[SDL_SCANCODE_Q]	    = ACTION_PHYSX_DBG;


	mMouseActionMap[SDL_BUTTON_LEFT]	= ACTION_SHOOT1;
	mMouseActionMap[SDL_BUTTON_RIGHT]	= ACTION_SHOOT2;
}

InputContext::~InputContext() {

}

void InputContext::SetAction(int keyCode, bool state) {
	//MyDbg(__FILE__, "mPrevAction[mKeyActionMap[keyCode]]: %d, state: %d", mPrevAction[mKeyActionMap[keyCode]], state);
	if (!mPrevAction[mKeyActionMap[keyCode]] && state) {
		mInputState.KeyDownAction[mKeyActionMap[keyCode]] = true;
	}
	else {
		mInputState.KeyDownAction[mKeyActionMap[keyCode]] = false;
	}

	mInputState.KeyPressed[mKeyActionMap[keyCode]] = state;

	mPrevAction[mKeyActionMap[keyCode]] = state;
}

void InputContext::SetMouseButton(int buttonCode, int state) {

	if (!mPrevAction[mMouseActionMap[buttonCode]] && state) {
		mInputState.KeyDownAction[mMouseActionMap[buttonCode]] = true;
	}
	else {
		mInputState.KeyDownAction[mMouseActionMap[buttonCode]] = false;
	}

	mInputState.KeyPressed[mMouseActionMap[buttonCode]] = state;


	mPrevAction[mMouseActionMap[buttonCode]] = !!state;
}

void InputContext::ClearActions() {
	for (int i = 0; i < ACTION_LAST_DUMMY; i++) {
		mKeyDownAction[i] = false;
		mKeyUpAction[i] = false;
		mPrevAction[i] = false;
	}
}

bool* InputContext::GetKeyDownStates() {
	return mKeyDownAction;
}

bool* InputContext::GetKeyUpStates() {
	return mKeyUpAction;
}

const InputState& InputContext::GetInputState() {
	return mInputState;
}

/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/

InputHandler::InputHandler() {
	mpInputListener = &mDummyListener;
	SetupMyGUIButtonMappings();
}

InputHandler::~InputHandler() {

}

const InputState& InputHandler::GetInputState() {
	return mInputContext.GetInputState();
}

void InputHandler::SetListener(InputEventListener *inputListener) {
	mpInputListener = inputListener;
	mListeners.clear();
	mListeners.push_back(inputListener);
}

void InputHandler::AddListener(InputEventListener *inputListener) {
	mListeners.push_back(inputListener);
}

bool InputHandler::CaptureEvent(SDL_Event &sdlEvent) {
	switch (sdlEvent.type) {

	case SDL_KEYDOWN:
		//MyDbg(__FILE__, "SDL_KEYDOWN keycode: %d", sdlEvent.key.keysym.scancode);
		MyGUI::InputManager::getInstance().injectKeyPress(mMyGUIKeyMap[sdlEvent.key.keysym.scancode]);
		mInputContext.SetAction(sdlEvent.key.keysym.scancode, true);
		for (size_t i = 0; i < mListeners.size(); i++) {
			mListeners[i]->OnKeyDown(mInputContext.GetInputState());
		}
		
		break;

	case SDL_KEYUP:
		//MyDbg(__FILE__, "SDL_KEYUP keycode: %d", sdlEvent.key.keysym.scancode);
		MyGUI::InputManager::getInstance().injectKeyRelease(mMyGUIKeyMap[sdlEvent.key.keysym.scancode]);
		mInputContext.SetAction(sdlEvent.key.keysym.scancode, false);
		for (size_t i = 0; i < mListeners.size(); i++) {
			mListeners[i]->OnKeyUp(mInputContext.GetInputState());
		}
		break;

	case SDL_MOUSEMOTION:
		mouseX = sdlEvent.motion.x;
		mouseY = sdlEvent.motion.y;
		MyGUI::InputManager::getInstance().injectMouseMove(mouseX, mouseY, 0);
		for (size_t i = 0; i < mListeners.size(); i++) {
			mListeners[i]->OnMouseMove(mouseX, mouseY, sdlEvent.motion.xrel, sdlEvent.motion.yrel);
		}
		break;

	case SDL_MOUSEBUTTONDOWN:
		//MyDbg(__FILE__, "MouseDown event: %d", sdlEvent.button.button);
		mouseX = sdlEvent.motion.x;
		mouseY = sdlEvent.motion.y;
		MyGUI::InputManager::getInstance().injectMousePress(mouseX, mouseY, mMyGUIMouseMap[sdlEvent.button.button]);
		mInputContext.SetMouseButton(sdlEvent.button.button, 1);
		for (size_t i = 0; i < mListeners.size(); i++) {
			mListeners[i]->OnMouseDown(mInputContext.GetInputState());
		}
		break;

	case SDL_MOUSEBUTTONUP:
		//MyDbg(__FILE__, "MouseUp event: %d", sdlEvent.button.button);
		mouseX = sdlEvent.motion.x;
		mouseY = sdlEvent.motion.y;
		MyGUI::InputManager::getInstance().injectMouseRelease(mouseX, mouseY, mMyGUIMouseMap[sdlEvent.button.button]);
		mInputContext.SetMouseButton(sdlEvent.button.button, 0);
		for (size_t i = 0; i < mListeners.size(); i++) {
			mListeners[i]->OnMouseUp(mInputContext.GetInputState());
		}
		break;

	case SDL_MOUSEWHEEL:
		break;

	case SDL_QUIT:
		return false;
		break;

	default:
		break;
	}
	
	return true;
}

void InputHandler::SetupMyGUIButtonMappings() {
	mMyGUIKeyMap.clear();
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_UNKNOWN, MyGUI::KeyCode::None));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_ESCAPE, MyGUI::KeyCode::Escape));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_1, MyGUI::KeyCode::One));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_2, MyGUI::KeyCode::Two));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_3, MyGUI::KeyCode::Three));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_4, MyGUI::KeyCode::Four));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_5, MyGUI::KeyCode::Five));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_6, MyGUI::KeyCode::Six));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_7, MyGUI::KeyCode::Seven));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_8, MyGUI::KeyCode::Eight));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_9, MyGUI::KeyCode::Nine));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_0, MyGUI::KeyCode::Zero));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_MINUS, MyGUI::KeyCode::Minus));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_EQUALS, MyGUI::KeyCode::Equals));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_BACKSPACE, MyGUI::KeyCode::Backspace));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_TAB, MyGUI::KeyCode::Tab));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_q, MyGUI::KeyCode::Q));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_w, MyGUI::KeyCode::W));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_e, MyGUI::KeyCode::E));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_r, MyGUI::KeyCode::R));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_t, MyGUI::KeyCode::T));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_y, MyGUI::KeyCode::Y));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_u, MyGUI::KeyCode::U));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_i, MyGUI::KeyCode::I));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_o, MyGUI::KeyCode::O));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_p, MyGUI::KeyCode::P));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_LEFTBRACKET, MyGUI::KeyCode::LeftBracket));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RIGHTBRACKET, MyGUI::KeyCode::RightBracket));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RETURN, MyGUI::KeyCode::Return));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_LCTRL, MyGUI::KeyCode::LeftControl));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_a, MyGUI::KeyCode::A));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_s, MyGUI::KeyCode::S));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_d, MyGUI::KeyCode::D));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_f, MyGUI::KeyCode::F));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_g, MyGUI::KeyCode::G));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_h, MyGUI::KeyCode::H));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_j, MyGUI::KeyCode::J));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_k, MyGUI::KeyCode::K));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_l, MyGUI::KeyCode::L));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_SEMICOLON, MyGUI::KeyCode::Semicolon));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_QUOTEDBL, MyGUI::KeyCode::Apostrophe));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_BACKQUOTE, MyGUI::KeyCode::Grave));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_LSHIFT, MyGUI::KeyCode::LeftShift));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_BACKSLASH, MyGUI::KeyCode::Backslash));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_z, MyGUI::KeyCode::Z));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_x, MyGUI::KeyCode::X));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_c, MyGUI::KeyCode::C));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_v, MyGUI::KeyCode::V));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_b, MyGUI::KeyCode::B));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_n, MyGUI::KeyCode::N));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_m, MyGUI::KeyCode::M));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_COMMA, MyGUI::KeyCode::Comma));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_PERIOD, MyGUI::KeyCode::Period));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_SLASH, MyGUI::KeyCode::Slash));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RSHIFT, MyGUI::KeyCode::RightShift));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_MULTIPLY, MyGUI::KeyCode::Multiply));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_LALT, MyGUI::KeyCode::LeftAlt));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_SPACE, MyGUI::KeyCode::Space));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_CAPSLOCK, MyGUI::KeyCode::Capital));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F1, MyGUI::KeyCode::F1));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F2, MyGUI::KeyCode::F2));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F3, MyGUI::KeyCode::F3));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F4, MyGUI::KeyCode::F4));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F5, MyGUI::KeyCode::F5));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F6, MyGUI::KeyCode::F6));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F7, MyGUI::KeyCode::F7));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F8, MyGUI::KeyCode::F8));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F9, MyGUI::KeyCode::F9));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F10, MyGUI::KeyCode::F10));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_NUMLOCKCLEAR, MyGUI::KeyCode::NumLock));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_SCROLLLOCK, MyGUI::KeyCode::ScrollLock));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_7, MyGUI::KeyCode::Numpad7));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_8, MyGUI::KeyCode::Numpad8));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_9, MyGUI::KeyCode::Numpad9));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_MINUS, MyGUI::KeyCode::Subtract));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_4, MyGUI::KeyCode::Numpad4));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_5, MyGUI::KeyCode::Numpad5));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_6, MyGUI::KeyCode::Numpad6));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_PLUS, MyGUI::KeyCode::Add));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_1, MyGUI::KeyCode::Numpad1));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_2, MyGUI::KeyCode::Numpad2));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_3, MyGUI::KeyCode::Numpad3));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_0, MyGUI::KeyCode::Numpad0));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_PERIOD, MyGUI::KeyCode::Decimal));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F11, MyGUI::KeyCode::F11));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F12, MyGUI::KeyCode::F12));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F13, MyGUI::KeyCode::F13));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F14, MyGUI::KeyCode::F14));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_F15, MyGUI::KeyCode::F15));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_EQUALS, MyGUI::KeyCode::NumpadEquals));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_DIVIDE, MyGUI::KeyCode::NumpadEnter));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RCTRL, MyGUI::KeyCode::RightControl));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_KP_DIVIDE, MyGUI::KeyCode::Divide));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_SYSREQ, MyGUI::KeyCode::SysRq));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RALT, MyGUI::KeyCode::RightAlt));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_PAUSE, MyGUI::KeyCode::Pause));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_HOME, MyGUI::KeyCode::Home));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_UP, MyGUI::KeyCode::ArrowUp));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_PAGEUP, MyGUI::KeyCode::PageUp));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_LEFT, MyGUI::KeyCode::ArrowLeft));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RIGHT, MyGUI::KeyCode::ArrowRight));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_END, MyGUI::KeyCode::End));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_DOWN, MyGUI::KeyCode::ArrowDown));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_PAGEDOWN, MyGUI::KeyCode::PageDown));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_INSERT, MyGUI::KeyCode::Insert));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_DELETE, MyGUI::KeyCode::Delete));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_LGUI, MyGUI::KeyCode::LeftWindows));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_RGUI, MyGUI::KeyCode::RightWindows));
	mMyGUIKeyMap.insert(std::pair<int, MyGUI::KeyCode>(SDLK_APPLICATION, MyGUI::KeyCode::AppMenu));

	mMyGUIMouseMap.clear();
	mMyGUIMouseMap.insert(std::pair<int, MyGUI::MouseButton>(SDL_BUTTON_LEFT, MyGUI::MouseButton::Left));
	mMyGUIMouseMap.insert(std::pair<int, MyGUI::MouseButton>(SDL_BUTTON_RIGHT, MyGUI::MouseButton::Right));
	mMyGUIMouseMap.insert(std::pair<int, MyGUI::MouseButton>(SDL_BUTTON_MIDDLE, MyGUI::MouseButton::Middle));
}