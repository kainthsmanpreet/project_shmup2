#include "LevelUtil.h"
#include "tinyxml2.h"
#include "EntityUtil.h"
#include "EntityTypes.h"

using namespace tinyxml2;

void ParseVector(std::stringstream &str, Ogre::Vector3 &inputVec) {
	std::string temp;
	int index = 0;
	std::getline(str, temp, ' ');
	inputVec[0] = std::stof(temp);
	std::getline(str, temp, ' ');
	inputVec[1] = std::stof(temp);
	inputVec[2] = 0.0;
}

LevelUtil::LevelUtil(EntityUtil *entUtl) {
	this->entityUtil = entUtl;
	this->currentLevel = 1;
}

void LevelUtil::LoadLevelFromXML(int levelNum) {
	std::stringstream levelName;
	levelName << "../data/level_" << levelNum << ".xml";
	SpawnerInfo spawnerInfo;
	Ogre::Vector3 spawnerPos;
	XMLDocument mainDoc;
	XMLElement *currElem;
	XMLError err = mainDoc.LoadFile(levelName.str().c_str());
	currElem = mainDoc.FirstChildElement("level")->FirstChildElement("spawners")->FirstChildElement();

	while (currElem != nullptr) {
		ParseVector(std::stringstream(currElem->Attribute("position")), spawnerPos);
		err = currElem->QueryIntAttribute("num_entities", &spawnerInfo.numEntities);
		err = currElem->QueryFloatAttribute("delay", &spawnerInfo.delayInSpawn);
		spawnerInfo.entityType = entityTypeMap.find(std::string(currElem->Attribute("entity_type")))->second;
		spawnerInfo.compToAttach = nullptr;
		PS_Component::BezierCurvePath *bezierCurve = nullptr;
		std::stringstream pathType(currElem->Attribute("path_type"));
		if (pathType.str().compare("PT_SPLINE") == 0) {
			std::vector<Ogre::Vector3> controlPts;
			XMLElement *controlPtElem = currElem->FirstChildElement();
			while (controlPtElem != nullptr) {
				Ogre::Vector3 ctrlPt;
				ParseVector(std::stringstream(controlPtElem->Attribute("position")), ctrlPt);
				controlPts.push_back(ctrlPt);
				controlPtElem = controlPtElem->NextSiblingElement();
			}
			bezierCurve = new BezierCurvePath(controlPts);
		}
		
		spawnerInfo.compToAttach = bezierCurve;
		entityUtil->CreateSpawnerEntity(spawnerPos, spawnerInfo);
		currElem = currElem->NextSiblingElement();
	}
}