#include"Component.h"
#include"CollisionEvent.h"

using namespace PS_Component;

void BoundaryCollision(Entity *self, Entity *other) {
	Collider *collider = dynamic_cast<Collider *>(other->getComponent<Collider>());
	if (!(collider->collisionGroup & CTAG_SPAWNER))
		other->remove();
}

void DamageCollision(Entity *self, Entity *other) {
	Health *otherHealth = dynamic_cast<Health *>(other->getComponent<Health>());
	Collider *otherCollider = dynamic_cast<Collider *>(other->getComponent<Collider>());

	Damage *selfDmg = dynamic_cast<Damage *>(self->getComponent<Damage>());

	if (!(otherCollider->collisionGroup & CTAG_BOUNDARY))
		otherHealth->mHealthValue -= selfDmg->DmgVal;
	self->remove();
}

void SpawnerCollision(Entity *self, Entity *other) {
	//self->removeComponent<Collider>();
	self->removeComponent<Kinematics>();
	self->addComponent(new ActivateSpawner());
	self->refresh();
}

void SimpleCollision(Entity *self, Entity *other) {

}
