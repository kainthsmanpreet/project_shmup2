#include "Player.h"
#include "CollisionEvent.h"

void Player::init(artemis::World *world, Graphics *graphics, btCollisionWorld *colWorld, EntityUtil *entityUtil) {
	MyDbg(__FILE__, "init()");
	mAccel = 110.0f;
	mDeAccel = 0.4f;
	mMaxSpeed = 200.0f;

	/*Init transform component*/
	Ogre::Quaternion quat; 
	quat.FromAngleAxis(Ogre::Degree(90.0f), Ogre::Vector3::UNIT_Y);
	mTransform = new PS_Component::Transform(Ogre::Vector3::ZERO, quat);
	mKinematics = new PS_Component::Kinematics(Ogre::Vector3::ZERO);
	//mKinematics = new PS_Component::Kinematics(0.0f);

	/*Init collider component*/
	mCollisionShape = new btBoxShape(btVector3(10.0f, 10.0f, 10.0f));
	btCollisionObject *obj = new btCollisionObject();
	obj->setCollisionShape(mCollisionShape);
	colWorld->addCollisionObject(obj, CTAG_PLAYER, 0);

	/*Create artemis entity*/
	artemis::Entity *ent = &world->createEntity();
	ent->addComponent(mTransform);
	ent->addComponent(mKinematics);
	ent->addComponent(new PS_Component::Renderer(
		graphics,
		graphics->AddSceneNode("test1node"),
		graphics->AddEntity("testEnt", "RZR-002.mesh"))
		);
	ent->addComponent(new PS_Component::Collider(colWorld, obj, SimpleCollision, ent, CTAG_PLAYER));
	ent->refresh();
	mEntWorld = world;
	mWeapon.SetEntityCreator(entityUtil);
}

void Player::OnKeyDown(const InputState &keyData) {
	MyDbg(__FILE__,"Player key down");
	Ogre::Vector3 velocity = Ogre::Vector3::ZERO;

	if (keyData.KeyPressed[ACTION_UP])
		velocity.y = mAccel;
	if (keyData.KeyPressed[ACTION_DOWN])
		velocity.y = -mAccel;
	if (keyData.KeyPressed[ACTION_LEFT])
		velocity.x = -mAccel;
	if (keyData.KeyPressed[ACTION_RIGHT])
		velocity.x = mAccel;

	mKinematics->mVelocity += velocity;
}

void Player::OnKeyUp(const InputState &keyData) {
	MyDbg(__FILE__, "Player key up");
	if (!keyData.KeyPressed[ACTION_UP] && !keyData.KeyPressed[ACTION_DOWN])
		mKinematics->mVelocity.y = 0.0f;
	if (!keyData.KeyPressed[ACTION_LEFT] && !keyData.KeyPressed[ACTION_RIGHT])
		mKinematics->mVelocity.x = 0.0f;

}

void Player::ProcessInput(const InputState &keyData) {
	Ogre::Vector3 velocity = Ogre::Vector3::ZERO;

	if (keyData.KeyPressed[ACTION_UP]) {
		/*velocity.y = mAccel;*/
		mKinematics->mVelocity.y += mAccel;
	}

	else if (keyData.KeyPressed[ACTION_DOWN]) {
		/*velocity.y = -mAccel;*/
		mKinematics->mVelocity.y += -mAccel;
	}

	else
		mKinematics->mVelocity.y *= mDeAccel;

	if (keyData.KeyPressed[ACTION_LEFT]) {
		/*velocity.x = -mAccel;*/
		mKinematics->mVelocity.x += -mAccel;
	}

	else if (keyData.KeyPressed[ACTION_RIGHT]) {
		/*velocity.x = mAccel;*/
		mKinematics->mVelocity.x += mAccel;
	}

	else
		mKinematics->mVelocity.x *= mDeAccel;

	if (keyData.KeyPressed[ACTION_SHOOT1]) {
		mWeapon.Shoot(mTransform->mPosition, mEntWorld->getDelta());
	}

	else {
		mWeapon.ResetShootingDelay();
	}
		
	if (keyData.KeyDownAction[ACTION_SHOOT2]) {
		//MyDbg(__FILE__, "right mouse button pressed");
		//mWeapon.SetCurrentWeapon(WeaponType::WT_RAPID_FIRE);
	}

	mKinematics->mVelocity.x = Ogre::Math::Clamp<Ogre::Real>(mKinematics->mVelocity.x, -mMaxSpeed, mMaxSpeed);
	mKinematics->mVelocity.y = Ogre::Math::Clamp<Ogre::Real>(mKinematics->mVelocity.y, -mMaxSpeed, mMaxSpeed);
	//mTransform->mVelocity += velocity;
}

void Player::SetScreenBounds(Ogre::Vector3 TopLeft, Ogre::Vector3 BottomRight) {
	topLeft = TopLeft;
	bottomRight = BottomRight;
}

void Player::SetWithinBounds() {
	mTransform->mPosition = Ogre::Vector3(
		Ogre::Math::Clamp<Ogre::Real>(mTransform->mPosition.x, topLeft.x, bottomRight.x),
		Ogre::Math::Clamp<Ogre::Real>(mTransform->mPosition.y, bottomRight.y, topLeft.y),
		mTransform->mPosition.z
		);
}

Player::~Player() {
	delete mCollisionShape;
}