#include "StateManager.h"

StateManager::~StateManager() {
	for (unsigned int i = 0; i < mActiveStack.size(); i++) {
		mActiveStack[i]->ExitState();
		//delete mActiveStack[i];
		//mActiveStack[i] =  NULL;
	}

	for (unsigned int i = 0; i < MAX_STATES; i++) {
		mStateStack[i]->destroy();
		//delete mStateStack[i];
		//mStateStack[i] = NULL;
	}

}

StateManager::StateManager() {
	mIsRunning = true;
	//mStateStack.clear();
	mActiveStack.clear();
}

void StateManager::AddNewState(StateId stateId, State *someState) {
	//mStateStack.push_back(someState);
	mStateStack[stateId] = someState;
}

void StateManager::PopState() {
	mActiveStack.back()->ExitState();
	mActiveStack.pop_back();
}

void StateManager::PushState(State* someState) {
	mActiveStack.push_back(someState);
	InitState(someState);
	mActiveStack.back()->EnterState();
	//mActiveStack.back()->EnterStateWithHandler(mpInputHandler);
}

void StateManager::PushState(StateId someId) {
	mActiveStack.push_back(mStateStack[someId]);
	InitState(mStateStack[someId]);
	mActiveStack.back()->EnterState();
	//mActiveStack.back()->EnterStateWithHandler(mpInputHandler);
}

void StateManager::SetState(State *someState) {
	PopState();
	PushState(someState);
}

void StateManager::SetState(StateId sId) {
	PopState();
	PushState(sId);
}

void StateManager::InitState(State *state) {
	mpInputHandler->SetListener(state);
	state->SetGraphicsSystem(mpGraphics);
	state->SetInputSystem(mpInputHandler);
}

void StateManager::Update() {
	if (mActiveStack.size() > 0)
		mActiveStack.back()->Update();
	else
		mIsRunning = false;
}

void StateManager::Update(float dt) {
	if (mActiveStack.size() > 0)
		mActiveStack.back()->Update(dt);
	else
		mIsRunning = false;
}

void StateManager::ResetStackAndPush(State *someState) {
	mActiveStack.back()->ExitState();
	mActiveStack.clear();
	mActiveStack.push_back(someState);
}

void StateManager::SetInputHandler(InputHandler *inputHandler) {
	this->mpInputHandler = inputHandler;
}

void StateManager::SetGraphicsSystem(Graphics *gfx) {
	this->mpGraphics = gfx;
}
