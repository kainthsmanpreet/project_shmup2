#include"EntityUtil.h"

std::unordered_map<std::string, long> ctagMap;
std::unordered_map<std::string, EntityType>	entityTypeMap;
std::unordered_map<std::string, CollisionDelegate> collisionCallBackMap;

EntityUtil::~EntityUtil() {
	MyDbg(__FILE__, "Destroying EntityUtil");
	MyDbg(__FILE__, "clearing shape cache");
	for (int i = 0; i < ET_LAST; i++) {
		delete mCollisionShapeCache[i];
	}

	MyDbg(__FILE__, "Clearing entity prototypes");
	for (int i = 0; i < ET_LAST; i++) {
		for (size_t j = 0; j < mEntityTemplates[i].components.size(); j++) {
			delete mEntityTemplates[i].components[j];
		}
		mEntityTemplates[i].components.clear();
	}
}

void EntityUtil::InitEntityTypeMapping() {
	entityTypeMap["ET_SPAWNER"]				= ET_SPAWNER;
	entityTypeMap["ET_ENEMY_RAZOR"]			= ET_ENEMY_RAZOR;
	entityTypeMap["ET_ENEMY_RAUD"]			= ET_ENEMY_RAUD;
	entityTypeMap["ET_STANDARD_BULLET"]		= ET_STANDARD_BULLET;
	entityTypeMap["ET_SPREAD_BULLET"]		= ET_SPREAD_BULLET;
	entityTypeMap["ET_BOUNDARY"]			= ET_BOUNDARY;
	entityTypeMap["ET_EXPLOSION_SIMPLE"]	= ET_EXPLOSION_SIMPLE;
	entityTypeMap["ET_LAST"]				= ET_LAST;

	collisionCallBackMap["BoundaryCollision"]	= BoundaryCollision;
	collisionCallBackMap["DamageCollision"]		= DamageCollision;
	collisionCallBackMap["SpawnerCollision"]	= SpawnerCollision;
	collisionCallBackMap["SimpleCollision"]		= SimpleCollision;

}

void EntityUtil::InitComponentStrMapping() {
	componentDeSerMap["Transform"]			= static_cast<DeSerializeDelegate>(&Transform::DeSerialize);
	componentDeSerMap["Kinematics"]			= static_cast<DeSerializeDelegate>(&Kinematics::DeSerialize);
	componentDeSerMap["Health"]				= static_cast<DeSerializeDelegate>(&Health::DeSerialize);
	//componentDeSerMap["Renderer"]			= static_cast<DeSerializeDelegate>(&Renderer::DeSerialize);
	//componentDeSerMap["Collider"]			= static_cast<DeSerializeDelegate>(&Collider::DeSerialize);
	componentDeSerMap["Damage"]				= static_cast<DeSerializeDelegate>(&Damage::DeSerialize);
	//componentDeSerMap["ParticleSystem"]		= static_cast<DeSerializeDelegate>(&ParticleSystem::DeSerialize);
	componentDeSerMap["ExpiryTimer"]		= static_cast<DeSerializeDelegate>(&ExpiryTimer::DeSerialize);
	componentDeSerMap["Spawner"]			= static_cast<DeSerializeDelegate>(&Spawner::DeSerialize);
	//componentDeSerMap["ActivateSpawner"]	= static_cast<DeSerializeDelegate>(&ActivateSpawner::DeSerialize);
	componentDeSerMap["BezierCurvePath"]	= static_cast<DeSerializeDelegate>(&BezierCurvePath::DeSerialize);
}

void EntityUtil::ParseEntityXML(const char *filename) {
	xmlDoc.LoadFile(filename);
	XMLElement *startEntNode = xmlDoc.FirstChildElement("entities")->FirstChildElement();
	XMLElement *currElem = startEntNode;
	while (currElem != NULL) {
		XMLElement *childElem = currElem->FirstChildElement();
		std::string entName(currElem->Attribute("type"));
		EntityType entType = entityTypeMap.find(entName)->second;
		MyDbg(__FILE__, "processing entity type %s", entName.c_str());
		while (childElem != NULL) {
			std::string compName(childElem->Value());
			MyDbg(__FILE__, "- processing component %s", compName.c_str());
			if (compName.compare("Collider") == 0) {
				mEntityTemplates[entType].components.push_back(Collider::DeSerialize(childElem, mColWorld));
			}
			else if (compName.compare("Renderer") == 0) {
				mEntityTemplates[entType].components.push_back(Renderer::DeSerialize(childElem, mGraphics));
			}
			else if (compName.compare("ParticleSystem") == 0) {
				mEntityTemplates[entType].components.push_back(ParticleSystem::DeSerialize(childElem, mGraphics));
			}
			else {
				auto res = componentDeSerMap.find(compName);
				mEntityTemplates[entType].components.push_back(res->second(childElem));
			}
			childElem = childElem->NextSiblingElement();
		}
		currElem = currElem->NextSiblingElement();
	}
}

void EntityUtil::InitPrototypes() {
	InitEntityTypeMapping();
	InitComponentStrMapping();
	ParseEntityXML("../data/entity_prototypes.xml");
}

void EntityUtil::CreateEntityFromType(EntityType entityType,	const Ogre::Vector3 &pos, 
																const Ogre::Quaternion &orientation,
																const Ogre::Vector3 &scale) {
	//MyDbg(__FILE__, "Entered CreateEntityFromType");
	artemis::Entity *tempEntity = &mEntityWorld->createEntity();
	tempEntity->addComponent(new Transform(pos, orientation, scale));
	int colGrp = 0, colMask = 0;
	switch (entityType){

	case ET_ENEMY_RAZOR: {
		colGrp = CTAG_ENEMY;
		colMask = CTAG_PLAYER_BULLET ;
		CheckShapeCache(entityType, btVector3(3.6f, 3.6f, 2.0f));
		btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[entityType], btVector3(pos.x, pos.y, pos.z), colGrp, colMask);
		tempEntity->addComponent(new Health(100));
		tempEntity->addComponent(new Kinematics(Ogre::Vector3(-70.0f, 0.0f, 0.0f), 0.0f, Ogre::Vector3(0.0f, 0.0f, 1.0f), 360.0f));
		tempEntity->addComponent(new Collider(mColWorld, colObj, SimpleCollision, tempEntity, colGrp));
		tempEntity->addComponent(new Renderer(
			mGraphics,
			mGraphics->AddSceneNode(pos),
			mGraphics->AddEntity("enemy_razor.mesh"))
			);
		tempEntity->addComponent(new ExpiryTimer(5.0f));
		break;
	}

	case ET_ENEMY_RAUD: {
		std::vector<Ogre::Vector3> ctrlPt;
		ctrlPt.push_back(Ogre::Vector3(58.96346f, 67.99773f, -234.02039f));
		ctrlPt.push_back(Ogre::Vector3(435.47418f, 57.74677f, -234.02045f));
		ctrlPt.push_back(Ogre::Vector3(96.24014f, -66.64467f, 0.0f));
		ctrlPt.push_back(Ogre::Vector3(44.80935f, 5.68727f, 0.0f));

		colGrp = CTAG_ENEMY;
		colMask = CTAG_PLAYER_BULLET;
		CheckShapeCache(entityType, btVector3(4.7f, 5.4f, 3.0f));
		btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[entityType], btVector3(pos.x, pos.y, pos.z), colGrp, colMask);
		tempEntity->addComponent(new Health(100));
		tempEntity->addComponent(new Kinematics(Ogre::Vector3(-70.0f, 0.0f, 0.0f), 0.0f, Ogre::Vector3(-1.0f, 0.0f, 0.0f), 360.0f));
		tempEntity->addComponent(new Collider(mColWorld, colObj, SimpleCollision, tempEntity, colGrp));
		tempEntity->addComponent(new Renderer(
			mGraphics,
			mGraphics->AddSceneNode(pos),
			mGraphics->AddEntity("Raud.mesh"))
			);
		tempEntity->addComponent(new BezierCurvePath(ctrlPt));
		tempEntity->addComponent(new ExpiryTimer(5.0f));
		break;
	}
	case ET_STANDARD_BULLET: {
		colGrp = CTAG_PLAYER_BULLET;
		colMask = CTAG_ENEMY | CTAG_BOUNDARY;
		CheckShapeCache(entityType, btVector3(2.3f, 0.8f, 0.3f));
		btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[entityType], btVector3(pos.x, pos.y, pos.z), colGrp, colMask);
		tempEntity->addComponent(new Kinematics(310.0f * (orientation * Ogre::Vector3::UNIT_X).normalisedCopy()));
		tempEntity->addComponent(new Health(100));
		tempEntity->addComponent(new Collider(mColWorld, colObj, DamageCollision, tempEntity, colGrp));
		tempEntity->addComponent(new Damage(45));
		tempEntity->addComponent(new ExpiryTimer(3.0f));
		Ogre::Entity *bulletEntity = mGraphics->AddEntity("BulletPlane.mesh");
		bulletEntity->setRenderQueueGroup(Ogre::RenderQueueGroupID::RENDER_QUEUE_9);
		tempEntity->addComponent(new Renderer(
			mGraphics,
			mGraphics->AddSceneNode(pos),
			bulletEntity)
			);
		break;
	}

	case ET_SPREAD_BULLET:{
		colGrp = CTAG_PLAYER_BULLET;
		colMask = CTAG_ENEMY | CTAG_BOUNDARY;
		CheckShapeCache(entityType, btVector3(1.8f, 0.9f, 0.4f));
		btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[entityType], btVector3(pos.x, pos.y, pos.z), colGrp, colMask);
		tempEntity->addComponent(new Kinematics(310.0f * (orientation * Ogre::Vector3::UNIT_X).normalisedCopy()));
		tempEntity->addComponent(new Health(100));
		tempEntity->addComponent(new Collider(mColWorld, colObj, DamageCollision, tempEntity, colGrp));
		tempEntity->addComponent(new Damage(13));
		tempEntity->addComponent(new ExpiryTimer(3.0f));
		Ogre::Entity *bulletEntity = mGraphics->AddEntity("BulletGreen.mesh");
		// bulletEntity->getMesh()->
		bulletEntity->setRenderQueueGroup(Ogre::RenderQueueGroupID::RENDER_QUEUE_9);
		tempEntity->addComponent(new Renderer(
			mGraphics,
			mGraphics->AddSceneNode(pos),
			bulletEntity)
			);
		break;
	}

	case ET_BOUNDARY: {
		colGrp = CTAG_BOUNDARY;
		colMask = CTAG_ALL & ~CTAG_PLAYER;
		btVector3 bound(mGraphics->worldBottomRight.x, 10.0f, 10.0f);
		btQuaternion quat(orientation.x, orientation.y, orientation.z, orientation.w);
		CheckShapeCache(entityType, bound);
		btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[entityType], btVector3(pos.x, pos.y, pos.z), quat, colGrp, colMask);
		tempEntity->addComponent(new Collider(mColWorld, colObj, BoundaryCollision, tempEntity, colGrp));
		break;
	}

	case ET_EXPLOSION_SIMPLE: {
		tempEntity->addComponent(new ParticleSystem(mGraphics, mGraphics->AddSceneNode(pos), mGraphics->AddParticleSystem("Explosion")));
		tempEntity->addComponent(new ExpiryTimer(1.5f));
		break;
	}

	default: {
		MyDbg(__FILE__, "Unknown entity type");
		break;
	}
	}
	tempEntity->refresh();
}

void EntityUtil::CreateEntityFromName(char* entityTypeName, Ogre::Vector3 pos) {
	MyDbg(__FILE__, "Entered CreateEntityFromName");
}

artemis::Entity* EntityUtil::CreateEntityFromTemplate(EntityType entType,	const Ogre::Vector3 &pos,
																const Ogre::Quaternion &orientation,
																const Ogre::Vector3 &scale) {
	artemis::Entity *tempEntity = &mEntityWorld->createEntity();
	tempEntity->addComponent(new Transform(pos, orientation, scale));
	for (size_t i = 0; i < mEntityTemplates[entType].components.size(); i++) {
		artemis::Component *comp = mEntityTemplates[entType].components[i]->Clone(tempEntity);
		tempEntity->addComponent(comp);
	}
	tempEntity->refresh();
	return tempEntity;
}

btCollisionObject* EntityUtil::CreateCollisionObject(btBoxShape* colShape, const btVector3 &pos, int group, int mask) {
	btTransform tempTransform;
	tempTransform.setIdentity();
	tempTransform.setOrigin(pos);
	btCollisionObject *colObj = new btCollisionObject();
	colObj->setCollisionShape(colShape);
	colObj->setWorldTransform(tempTransform);
	mColWorld->addCollisionObject(colObj, group, mask);
	return colObj;
}

btCollisionObject* EntityUtil::CreateCollisionObject(btBoxShape* colShape, const btVector3 &pos, const btQuaternion &quat, int group, int mask) {
	btTransform tempTransform;
	tempTransform.setIdentity();
	tempTransform.setOrigin(pos);
	tempTransform.setRotation(quat);
	btCollisionObject *colObj = new btCollisionObject();
	colObj->setCollisionShape(colShape);
	colObj->setWorldTransform(tempTransform);
	mColWorld->addCollisionObject(colObj, group, mask);
	return colObj;
}

void EntityUtil::CheckShapeCache(EntityType etType, const btVector3 &bound) {
	if (mCollisionShapeCache[etType] == NULL) {
		mCollisionShapeCache[etType] = new btBoxShape(bound);
	}
}

void EntityUtil::DestroyEntity(Entity *e) {
	e->remove();
}

void EntityUtil::CreateSpawnerEntity(Ogre::Vector3 pos, SpawnerInfo &spawnInfo) {
	int colGrp = CTAG_SPAWNER;
	int colMask = CTAG_BOUNDARY;
	CheckShapeCache(ET_SPAWNER, btVector3(10.0f, 10.0f, 10.0f));
	btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[ET_SPAWNER], btVector3(pos.x, pos.y, pos.z), colGrp, colMask);
	artemis::Entity *tempEntity = &mEntityWorld->createEntity();
	tempEntity->addComponent(new Transform(pos));
	tempEntity->addComponent(new Kinematics(Ogre::Vector3(-30.0f, 0.0f, 0.0f)));
	tempEntity->addComponent(new Collider(mColWorld, colObj, SpawnerCollision, tempEntity, colGrp));
	tempEntity->addComponent(new Renderer(mGraphics, mGraphics->AddSceneNode(pos), mGraphics->AddEntity("Barrel.mesh")));
	tempEntity->addComponent(new Spawner(spawnInfo.numEntities, spawnInfo.delayInSpawn, spawnInfo.entityType, spawnInfo.speed, spawnInfo.compToAttach));
	tempEntity->refresh();
}

void EntityUtil::CreateSpawnerEntity(Ogre::Vector3 pos, int num, float delay, EntityType entType, float speed) {
	int colGrp = CTAG_SPAWNER;
	int colMask = CTAG_BOUNDARY;
	CheckShapeCache(ET_SPAWNER, btVector3(10.0f, 10.0f, 10.0f));
	btCollisionObject *colObj = CreateCollisionObject(mCollisionShapeCache[ET_SPAWNER], btVector3(pos.x, pos.y, pos.z), colGrp, colMask);
	artemis::Entity *tempEntity = &mEntityWorld->createEntity();
	tempEntity->addComponent(new Transform(pos));
	tempEntity->addComponent(new Kinematics(Ogre::Vector3(-20.0f, 0.0f, 0.0f)));
	tempEntity->addComponent(new Collider(mColWorld, colObj, SpawnerCollision, tempEntity, colGrp));
	tempEntity->addComponent(new Renderer(mGraphics, mGraphics->AddSceneNode(pos), mGraphics->AddEntity("Barrel.mesh")));
	tempEntity->addComponent(new Spawner(num, delay, entType, speed, nullptr));
	tempEntity->refresh();
}