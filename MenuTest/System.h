#include "Component.h"
#include "Artemis\ComponentMapper.h"
#include "Artemis\EntityProcessingSystem.h"
#include "EntityUtil.h"

#include <iostream>
using namespace artemis;

namespace PS_System {
	
	class TransformUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<PS_Component::Transform> mTransformMapper;
		ComponentMapper<PS_Component::Kinematics> mKinematicsMapper;
	public:
		TransformUpdate() {
			addComponentType<PS_Component::Transform>();
			addComponentType<PS_Component::Kinematics>();
		}

		void initialize() {
			mTransformMapper.init(*world);
			mKinematicsMapper.init(*world);
		}

		void processEntity(Entity &e) {
			PS_Component::Transform *tf = mTransformMapper.get(e);
			PS_Component::Kinematics *km = mKinematicsMapper.get(e);						
			if (km->mSpeed == 0.0f)
				tf->mPosition += km->mVelocity * world->getDelta();
			else {
				Ogre::Vector3 velocity = Ogre::Quaternion(tf->mOrientation.getRoll(), Ogre::Vector3::UNIT_Z) * Ogre::Vector3::UNIT_X;
				velocity.normalise();
				tf->mPosition += km->mSpeed * world->getDelta() * velocity;
			}
			//km->mCurrAng += km->mRotSpeed * world->getDelta();
			tf->mOrientation = tf->mOrientation * Ogre::Quaternion(Ogre::Degree(world->getDelta() * km->mRotSpeed), km->mRotDir);
		}
	};

	class RenderUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<PS_Component::Renderer> mRenderMapper;
		ComponentMapper<PS_Component::Transform> mTransformMapper;

	public:
		RenderUpdate() {
			addComponentType<PS_Component::Renderer>();
			addComponentType<PS_Component::Transform>();
			addComponentType<PS_Component::Kinematics>();
		}

		void initialize() {
			mRenderMapper.init(*world);
			mTransformMapper.init(*world);
		}

		void processEntity(Entity &e) {
			PS_Component::Renderer *renderer = mRenderMapper.get(e);
			PS_Component::Transform *transform = mTransformMapper.get(e);

			renderer->mSceneNode->setPosition(transform->mPosition);
			renderer->mSceneNode->setOrientation(transform->mOrientation);
		}

	};

	class ParticleSystemUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<Transform> tfMapper;
		ComponentMapper<ParticleSystem> psMapper;
	public:
		ParticleSystemUpdate() {
			addComponentType<Transform>();
			addComponentType<ParticleSystem>();
		}

		void initialize() {
			tfMapper.init(*world);
			psMapper.init(*world);
		}

		void processEntity(Entity &e) {
			psMapper.get(e)->sceneNode->setPosition(tfMapper.get(e)->mPosition);
		}
	};

	class PhysicsUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<PS_Component::Collider> mColliderMapper;
		ComponentMapper<PS_Component::Transform> mTransformMapper;
	public:
		PhysicsUpdate() {
			addComponentType<PS_Component::Collider>();
			addComponentType<PS_Component::Transform>();
			addComponentType<PS_Component::Kinematics>();
		}

		void initialize() {
			mColliderMapper.init(*world);
			mTransformMapper.init(*world);
		}

		void processEntity(Entity &e) {
			PS_Component::Transform *transform = mTransformMapper.get(e);
			PS_Component::Collider	*collider = mColliderMapper.get(e);

			btVector3 pos(transform->mPosition.x, transform->mPosition.y, transform->mPosition.z);
			btQuaternion quat(transform->mOrientation.x, transform->mOrientation.y, transform->mOrientation.z, transform->mOrientation.w);
			btTransform colliderTransform;// = collider->mCollisionObject->getWorldTransform();
			colliderTransform.setIdentity();
			colliderTransform.setOrigin(pos);
			colliderTransform.setRotation(quat);

			collider->mCollisionObject->setWorldTransform(colliderTransform);
		}
	};

	class CheckHealth : public EntityProcessingSystem {
	private:
		ComponentMapper<PS_Component::Health> healthMapper;
		ComponentMapper<PS_Component::Transform> transformMapper;
		EntityUtil *entityUtil;
		CheckHealth() {}
	public:
		CheckHealth(EntityUtil *entUtl) {
			addComponentType<PS_Component::Health>();
			addComponentType<PS_Component::Transform>();

			entityUtil = entUtl;
		}

		void initialize() {
			healthMapper.init(*world);
			transformMapper.init(*world);
		}

		void processEntity(Entity &e) {
			if (healthMapper.get(e)->mHealthValue <= 0) {
				e.remove();
				//entityUtil->CreateEntityFromType(EntityType::ET_EXPLOSION_SIMPLE, transformMapper.get(e)->mPosition);
				entityUtil->CreateEntityFromTemplate(EntityType::ET_EXPLOSION_SIMPLE, transformMapper.get(e)->mPosition);
			}
		}
	};

	class ExpiryUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<PS_Component::ExpiryTimer> expiryMapper;
	public:
		ExpiryUpdate() {
			addComponentType<PS_Component::ExpiryTimer>();
		}

		void initialize() {
			expiryMapper.init(*world);
		}

		void processEntity(Entity &e) {
			PS_Component::ExpiryTimer *expire = expiryMapper.get(e);			
			expire->counter += world->getDelta();
			if (expire->counter >= expire->timeToLive) {
				e.remove();
			}
		}
	};

	class SpawnerUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<Spawner> spMapper; 
		ComponentMapper<Transform> tfMapper;
		EntityUtil	*entityUtil;
		SpawnerUpdate() {}
	public:
		SpawnerUpdate(EntityUtil *entUtl) {
			addComponentType<Transform>();
			addComponentType<Spawner>();
			addComponentType<ActivateSpawner>();
			entityUtil = entUtl;
		}

		void initialize() {
			spMapper.init(*world); 
			tfMapper.init(*world);
		}

		void processEntity(Entity &e) {
			PS_Component::Spawner *spawner = spMapper.get(e);
			PS_Component::Transform *tf = tfMapper.get(e);
			spawner->delayCounter += world->getDelta();
			if (spawner->delayCounter >= spawner->delayInSpawn) {
				if (spawner->entityType != EntityType::ET_BOUNDARY) {
					Entity *ent = entityUtil->CreateEntityFromTemplate(spawner->entityType, tf->mPosition);
					ent->addComponent(spawner->addComponentOnSpawn->Clone(ent));
				}
				else {
					entityUtil->CreateEntityFromType(spawner->entityType, tf->mPosition);
				}
				spawner->delayCounter = 0.0f;
				spawner->numLeftToSpawn--;
				//spawner->SpawnEntity();
			}
			
			if (spawner->numLeftToSpawn <= 0) {
				e.remove();
				return;
			}			
		}
	};

	class SplineUpdate : public EntityProcessingSystem {
	private:
		ComponentMapper<BezierCurvePath> splineMapper;
		ComponentMapper<Transform> tfMapper;
	public:
		SplineUpdate() {
			addComponentType<Transform>();
			addComponentType<BezierCurvePath>();
		}

		void initialize() {
			tfMapper.init(*world);
			splineMapper.init(*world);
		}

		void processEntity(Entity &e) {
			BezierCurvePath *bz = splineMapper.get(e);
			bz->t += 0.5f * world->getDelta();
			if (bz->t >= 1.0f && bz->isLoop)
				bz->t = 0.0f;
			tfMapper.get(e)->mPosition = bz->GetPointAtT(bz->t);
		}
	};
}