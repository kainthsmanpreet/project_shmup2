
#ifndef GAMESTATE_H
#define GAMESTATE_H
#include "Graphics.h"
#include "Input.h"

class State;
/*	
 *	This class is defined here mainly because state_manager's implementation
 *	needs to know the state class structure. 
 */

enum StateId
{
	MAIN_MENU,
	PAUSE_MENU,
	IN_GAME
};

class StateManagerAbstract {
public:

	/*Push a new state*/
	virtual void PushState(State *) = 0;

	/*Push a new state identified by state id*/
	virtual void PushState(StateId) = 0;

	/*Remove the topmost state*/
	virtual void PopState() = 0;

	/*Change the state*/
	virtual void SetState(State *) = 0;

	virtual void SetState(StateId) = 0;

	virtual void AddNewState(StateId, State *) = 0;

	virtual void Update() = 0;
};


class State : public InputEventListener {
public:
	/*Push a state on top of this one*/
	void PushState(State *state) { mStateManager->PushState(state); }

	/*Tell statemanager to push this state*/
	void PushState(StateId stateId) { mStateManager->PushState(stateId); }

	/*Tell statemanager to change topmost state*/
	void SetState(State *state) { mStateManager->SetState(state); }

	void SetState(StateId sId) { mStateManager->SetState(sId); }

	/*Tell statemanager to pop topmost active state*/
	void PopState() { mStateManager->PopState(); }

	void SetGraphicsSystem(Graphics *gfx) {
		this->graphics	= gfx;
		this->gui		= gfx->GetGUIPtr();
	}

	void SetInputSystem(InputHandler *input) {
		this->mInputHandler = input;
	}

	/*
	********************************************************
	* Virtuals, all derived classes must implement
	********************************************************
	*/

	//virtual ~State() { std::cout << "Base state destructor\n"; }

	virtual void EnterState() = 0;

	virtual void EnterStateWithHandler(InputHandler*) { };

	virtual void ExitState() = 0;

	virtual void Update() = 0;

	virtual void Update(float dt) = 0;
	
	void destroy() { delete this; }

protected:
	/*Pointer to the graphics system*/
	Graphics *graphics;

	/*Pointer to the GUI instance */
	MyGUI::Gui *gui;

	/*Pointer to the parent state manager*/
	StateManagerAbstract *mStateManager;

	/*Pointer to the input handler*/
	InputHandler *mInputHandler;

private:
};

#define DECLARE_SINGLETON(CLASS) \
	static void CreateState(StateId stateId, StateManagerAbstract *parent) {	\
		CLASS *classInstance = new CLASS();										\
		classInstance->mStateManager = parent;									\
		parent->AddNewState(stateId, classInstance);							\
	}																			\

#endif