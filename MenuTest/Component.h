#pragma once
#include "Artemis\Entity.h"
#include "Artemis\Component.h"
#include "BulletCollision\CollisionDispatch\btCollisionWorld.h"
#include "BulletCollision\CollisionShapes\btBoxShape.h"
#include "OgreSceneNode.h"
#include "OgreEntity.h"
#include "OgreVector3.h"
#include "OgreRenderQueue.h"
#include "BulletCollision\CollisionDispatch\btCollisionObject.h"
#include "Dbg.h"
#include "Graphics.h"
#include "EntityTypes.h"
#include "CollisionTags.h"
#include "CollisionEvent.h"
#include "tinyxml2.h"

#include <iostream>
#include <sstream>
#include <string>

using namespace artemis;
using namespace tinyxml2;

typedef void		(*CollisionCallBack)(Entity *, Entity *);
typedef void		(*SpawnEntityDelegate)();
typedef Component*	(*DeSerializeDelegate)(tinyxml2::XMLElement *);

#ifndef CLONE_FUNC
#define CLONE_FUNC(Type, ...)							\
	Component* Clone(artemis::Entity *e) {				\
		return new Type(__VA_ARGS__);					\
	}	
#endif

template <typename T>
void XMLParseVector(std::stringstream &str, T &inputVec) {
	std::string temp;
	int index = 0;
	while(std::getline(str, temp, ' ')) {
		inputVec[index++] = std::stof(temp);
	}
}

//template<typename T>
//void RegisterSerializeFunc() {
//	componentSerializeMap[STRINGIFY(T)] = static_cast<DeSerializeDelegate>(&T::DeSerialize);
//}

//#ifndef REGISTER_DESER_FUNC
//#define REGISTER_DESER_FUNC(Typename)	\
//	componentSerializeMap[#Typename] = static_cast<DeSerializeDelegate>(&Typename::DeSerialize);
//#endif

namespace PS_Component {

	class Transform : public Component {
	private:
	public:
		Ogre::Vector3		mPosition; 
		Ogre::Quaternion	mOrientation;
		Ogre::Vector3		mScale;

		Transform() :
			mPosition(Ogre::Vector3::ZERO),
			mOrientation(Ogre::Quaternion::IDENTITY),
			mScale(Ogre::Vector3::UNIT_SCALE)
		{

		}

		Transform(
			Ogre::Vector3 pos,
			Ogre::Quaternion orientation = Ogre::Quaternion::IDENTITY,
			Ogre::Vector3 scale = Ogre::Vector3::UNIT_SCALE
			) :
			mPosition(pos),
			mOrientation(orientation),
			mScale(scale)
		{

		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "transform deserialize");
			return new Transform();
		}

		CLONE_FUNC(Transform);
	};

	class Kinematics : public Component {
	private:
	public:
		Ogre::Vector3		mVelocity;
		float				mSpeed;
		Ogre::Vector3		mRotDir;
		float				mRotSpeed;
		float				mCurrAng;

		Kinematics() : 
			mRotDir(Ogre::Vector3::ZERO)
		{
			mSpeed = 0.0f;
		}

		Kinematics(
			Ogre::Vector3 velocity,
			float speed = 0.0f,
			Ogre::Vector3 rotVelocity = Ogre::Vector3::ZERO,
			float rotOmega = 0.0f
			) :
			mVelocity(velocity),
			mRotDir(rotVelocity.normalisedCopy())
		{
			mSpeed = speed;
			mRotSpeed = rotOmega;
			mCurrAng = 0.0f;
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "kinematics deserialize");
			const char* velocityStr = "0.0 0.0 0.0";
			const char* speedStr = "0.0";

			if (xmlElem->Attribute("velocity") != NULL)
				velocityStr = xmlElem->Attribute("velocity");
			if (xmlElem->Attribute("speed") != NULL)
				speedStr = xmlElem->Attribute("speed");
			
			std::stringstream rotDir(xmlElem->Attribute("rotation_axis"));
			std::stringstream rotSpeed(xmlElem->Attribute("rotation_speed"));

			Ogre::Vector3 vel, rotAxis;
			float speedVal = (float)atof(speedStr);
			float w = std::stof(rotSpeed.str());;
			XMLParseVector<Ogre::Vector3>(std::stringstream(velocityStr), vel);
			XMLParseVector<Ogre::Vector3>(rotDir, rotAxis);

			return new Kinematics(vel, speedVal, rotAxis, w);
		}

		CLONE_FUNC(Kinematics, mVelocity, mSpeed, mRotDir, mRotSpeed);
	};

	class Health : public Component {
	public:
		int mHealthValue;

		Health() : mHealthValue(100) {

		}
	
		Health(int val) : mHealthValue(val) {
			 
		}

		void DoDamage(int val) {
			MyDbg(__FILE__, "DoDamage called, dmg: %d, hp left: %d", val, mHealthValue);
			mHealthValue -= val;
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "health deserialize");
			std::string health(xmlElem->Attribute("value"));
			return new Health(std::stoi(health));
		}
		
		CLONE_FUNC(Health, mHealthValue);
	};

	class Renderer : public Component {
	private:
		Renderer() {}
	public:
		Graphics		*mGraphics;
		Ogre::SceneNode	*mSceneNode;
		Ogre::Entity	*mEntity;

		Renderer(Graphics *gfx, Ogre::Entity *createdEntity) : mGraphics(gfx), mEntity(createdEntity) 
		{

		}

		Renderer(Graphics *graphics, Ogre::SceneNode *createdNode, Ogre::Entity	*createdEntity) :
			mGraphics(graphics), mSceneNode(createdNode), mEntity(createdEntity)
		{
			mSceneNode->attachObject(mEntity);
		}

		Renderer(
			Ogre::SceneNode *createdNode, 
			Ogre::Entity	*createdEntity	) : 
			mSceneNode(createdNode),
			mEntity(createdEntity) 
		{
			mSceneNode->attachObject(mEntity);
		}

		~Renderer() 
		{
			mSceneNode->detachObject(mEntity);
			//mGraphics->ogreSceneManager->destroyEntity(mEntity);
			mGraphics->ogreSceneManager->destroySceneNode(mSceneNode);
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem, Graphics *gfx) {
			MyDbg(__FILE__, "renderer deserialize");
			Ogre::String meshName(xmlElem->Attribute("mesh_name"));
			const char *renderQueueStr = xmlElem->Attribute("render_queue");
			Ogre::uint8 renderQueueId = Ogre::RenderQueueGroupID::RENDER_QUEUE_MAIN;
			if (renderQueueStr != NULL)
				renderQueueId = atoi(renderQueueStr);

			Ogre::SceneNode *newNode = gfx->AddSceneNode(-999.0f * Ogre::Vector3::UNIT_SCALE);
			Ogre::Entity *newEntity = gfx->AddEntity(meshName);
			newEntity->setRenderQueueGroup(renderQueueId);
			return new Renderer(gfx, 
								newNode,
								newEntity);
		}

		Component* Clone(Entity *e) {
			Ogre::SceneNode *newSceneNode= mGraphics->AddSceneNode(mSceneNode->getPosition());
			Ogre::Entity *newEntityInstance = mGraphics->AddEntity(mEntity->getMesh());
			newEntityInstance->setRenderQueueGroup(mEntity->getRenderQueueGroup());
			return new Renderer(mGraphics, newSceneNode, newEntityInstance);
		}
	};

	class Collider : public Component {
	private:
		Collider() {}
	public:
		btCollisionWorld	*mCollisionWorld;
		btCollisionObject	*mCollisionObject;
		/*CollisionEvent		*mCollisionEvent;*/
		Entity				*mEntity;

		CollisionCallBack	OnCollision;

		int					collisionGroup;
		int					collisionMask;

		btCollisionShape *mCollisionShape;

		//Collider(btCollisionWorld *colWorld, btCollisionObject *colObj, CollisionCallBack callback) :
		//	mCollisionWorld(colWorld), mCollisionObject(colObj)
		//{
		//	OnCollision = callback;
		//	mCollisionObject->setUserPointer(this);
		//	collisionGroup = mCollisionObject->getBroadphaseHandle()->m_collisionFilterGroup ;
		//	mCollisionWorld->removeCollisionObject(mCollisionObject);
		//}

		Collider(btCollisionWorld *colWorld, btCollisionObject *colObj, CollisionCallBack callback, Entity *e, int collisionGrp) :
			mCollisionWorld(colWorld), mCollisionObject(colObj)
		{
			OnCollision = callback;
			mCollisionObject->setUserPointer(this);
			mEntity = e; 
			collisionGroup = collisionGrp;
		}

		Collider(btCollisionWorld *colWorld, btCollisionObject *colObj, CollisionCallBack callback, int colGrp, int colMask) :
			mCollisionWorld(colWorld), mCollisionObject(colObj)
		{
			OnCollision = callback;
			mCollisionObject->setUserPointer(this);
			collisionGroup = colGrp;
			collisionMask = colMask;
		}

		~Collider() 
		{
			mCollisionWorld->removeCollisionObject(mCollisionObject);
			delete mCollisionObject;
			delete mCollisionShape;
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem, btCollisionWorld *collisionWorld) {
			MyDbg(__FILE__, "collider deserialize");
			//std::string colShp(xmlElem->Attribute("shape"));
			std::string mask(xmlElem->Attribute("mask"));
			std::string group(xmlElem->Attribute("group"));
			std::string callback(xmlElem->Attribute("callback"));

			std::stringstream dimensions(xmlElem->Attribute("dimensions"));
			btVector3 extent;
			XMLParseVector<btVector3>(dimensions, extent);
			btCollisionObject *newColObj = new btCollisionObject();

			/*TODO: Do something about prototype components ending up in the active world*/
			btTransform newTransform;
			newTransform.setOrigin(-999.0f * btVector3(1.0f, 1.0f, 1.0f));
			newTransform.setRotation(btQuaternion::getIdentity());
			
			/*TODO: Allow other collision shapes*/
			/*TODO: fix this mem leak here*/
			btCollisionShape *newColShape = new btBoxShape(extent);
			CollisionDelegate collisionHandler = collisionCallBackMap.find(callback)->second;
			
			newColObj->setCollisionShape(newColShape);
			newColObj->setWorldTransform(newTransform);
			
			//collisionWorld->addCollisionObject(newColObj, std::stoi(group), std::stoi(mask));
			Collider *newCollider = new Collider(collisionWorld, newColObj, collisionHandler, std::stoi(group), std::stoi(mask));
			newCollider->mCollisionShape = newColShape;
			return newCollider;
		}

		Component* Clone(artemis::Entity *e) {
			btCollisionObject *newColObj = new btCollisionObject();
			//btBroadphaseProxy *bpHandle = mCollisionObject->getBroadphaseHandle();
			btCollisionShape *colShape = mCollisionObject->getCollisionShape();
			newColObj->setCollisionShape(colShape);
			newColObj->setWorldTransform(mCollisionObject->getWorldTransform());
			//mCollisionWorld->addCollisionObject(newColObj, bpHandle->m_collisionFilterGroup, bpHandle->m_collisionFilterMask);
			mCollisionWorld->addCollisionObject(newColObj, collisionGroup, collisionMask);
			return new Collider(mCollisionWorld, newColObj, OnCollision, e, collisionGroup);
		}

	};

	class Damage : public Component {
	private:
	public:
		int DmgVal;
		Damage() {
			DmgVal = 1;
		}
		Damage(int val) {
			DmgVal = val;
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "damage deserialize");
			std::string dmg(xmlElem->Attribute("value"));
			return new Damage(std::stoi(dmg));
		}

		CLONE_FUNC(Damage, DmgVal);
	};

	class ParticleSystem : public Component {
	private:
		ParticleSystem() {}
	public:
		Graphics	*graphics;
		Ogre::SceneNode *sceneNode;
		Ogre::ParticleSystem *particleSystem;
		Ogre::String templateName;

		ParticleSystem(Graphics *gfx, Ogre::SceneNode *node, Ogre::String particleSysName) :
			graphics(gfx), sceneNode(node), templateName(particleSysName)
		{	
			particleSystem = graphics->AddParticleSystem(templateName);
			sceneNode->attachObject(particleSystem);
		}

		ParticleSystem(Graphics *gfx, Ogre::SceneNode *node, Ogre::ParticleSystem *particleSys) :
			graphics(gfx), sceneNode(node), particleSystem(particleSys)			
		{
			sceneNode->attachObject(particleSys);
		}

		~ParticleSystem() {
 			sceneNode->detachObject(particleSystem);
			graphics->ogreSceneManager->destroySceneNode(sceneNode);
			graphics->ogreSceneManager->destroyParticleSystem(particleSystem);
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem, Graphics *graphics) {
			MyDbg(__FILE__, "particle sys deserialize");
			std::string particleFXName(xmlElem->Attribute("name"));
			Ogre::SceneNode *newNode = graphics->AddSceneNode(-999.0f * Ogre::Vector3::UNIT_SCALE);
			Ogre::ParticleSystem *newParticleFx = graphics->AddParticleSystem(particleFXName);
			newNode->setVisible(false);
			newParticleFx->setVisible(false);
			return new ParticleSystem(graphics, newNode, particleFXName);
		}

		Component* Clone(artemis::Entity *e) {
			//Ogre::SceneNode *newSceneNode = graphics->AddSceneNode(sceneNode->getPosition());
			Transform *tf = dynamic_cast<Transform *>(e->getComponent<Transform>());
			Ogre::SceneNode *newSceneNode = graphics->AddSceneNode(tf->mPosition);
			Ogre::ParticleSystem *newParticleFx = graphics->AddParticleSystem(templateName);
			return new ParticleSystem(graphics, newSceneNode, newParticleFx);
		}

		//CLONE_FUNC(ParticleSystem, graphics, sceneNode, particleSystem);
	};

	class ExpiryTimer : public Component {
	private:
		ExpiryTimer() {}
	public:
		float counter;
		float timeToLive;
		ExpiryTimer(float val) {
			counter = 0.0f;
			timeToLive = val;
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "expiry deserialize");
			std::string expTimer(xmlElem->Attribute("value"));
			return new ExpiryTimer(std::stof(expTimer));
		}

		CLONE_FUNC(ExpiryTimer, timeToLive);
	};

	class Spawner : public Component {
	private:
		Spawner() {}
	public:
		int numLeftToSpawn;
		float delayCounter;

		int numEntities;
		float delayInSpawn;
		EntityType entityType;
		float entitySpeed;
		
		Component *addComponentOnSpawn;

		Spawner(int num, float delay, EntityType entType, float speed, Component *attachComp) {
			numEntities = numLeftToSpawn = num;
			delayInSpawn = delayCounter = delay;
			entityType = entType;
			entitySpeed = speed;
			addComponentOnSpawn = attachComp;
		}

		~Spawner() {
			delete addComponentOnSpawn;
		}

		SpawnEntityDelegate SpawnEntity;

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "spawner deserialize");
			return new Spawner();
		}

		CLONE_FUNC(Spawner, numEntities, delayInSpawn, entityType, entitySpeed, addComponentOnSpawn);
	};

	class ActivateSpawner : public Component {
		CLONE_FUNC(Transform);
	};

	class BezierCurvePath : public Component {
	private:
		BezierCurvePath() {}
	public:
		std::vector<Ogre::Vector3> controlPoints;
		Ogre::Vector3 offset;
		float t;
		bool isLoop;
		BezierCurvePath(std::vector<Ogre::Vector3> pts/*, Ogre::Vector3 ofst*/) {
			controlPoints = pts;
			//offset = ofst;
			t = 0.0f;
			isLoop = true;
		}

		Ogre::Vector3 GetPointAtT(float param) {
			float OneMinusT = 1 - param;
			return
				OneMinusT * OneMinusT * OneMinusT * controlPoints[0] +
				3.0f * OneMinusT * OneMinusT * param * controlPoints[1] +
				3.0f * OneMinusT * param * param * controlPoints[2] +
				param * param * param * controlPoints[3];
		}

		static Component* DeSerialize(tinyxml2::XMLElement *xmlElem) {
			MyDbg(__FILE__, "bezier curve deserialize");
			return new BezierCurvePath();
		}

		CLONE_FUNC(BezierCurvePath, controlPoints);

	};
};